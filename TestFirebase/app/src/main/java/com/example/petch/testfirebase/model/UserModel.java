package com.example.petch.testfirebase.model;

import android.content.Context;

public class UserModel {

    private Context context;
    private String key;
    private String email;
    private String username;
    private String password;
    private String repeat_Password;


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeat_Password() {
        return repeat_Password;
    }

    public void setRepeat_Password(String repeat_Password) {
        this.repeat_Password = repeat_Password;
    }

    //    //เพิ่ม
//    private String deviceId;
//    private String deviceToken;
//
//    public String getDeviceId() {
//        return deviceId;
//    }
//
//    public void setDeviceId(String deviceId) {
//        this.deviceId = deviceId;
//    }
//
//    public String getDeviceToken() {
//        return deviceToken;
//    }
//
//    public void setDeviceToken(String deviceToken) {
//        this.deviceToken = deviceToken;
//    }
//    //


}

