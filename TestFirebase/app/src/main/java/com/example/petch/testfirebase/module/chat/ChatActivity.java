package com.example.petch.testfirebase.module.chat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.MessageModel;
import com.example.petch.testfirebase.model.UserModel;
import com.example.petch.testfirebase.module.image.ImageActivity;
import com.example.petch.testfirebase.module.main.MainActivity;
import com.example.petch.testfirebase.module.manage.ManageActivity;
import com.example.petch.testfirebase.module.member.MemberActivity;
import com.example.petch.testfirebase.realm.RealmManage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatActivity extends AppCompatActivity implements ChatConstructor.View, ChatConstructor.DelChatMessage
        , NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, View.OnTouchListener {

    @BindView(R.id.chat_menu)
    ImageView chat_menu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_chat)
    RecyclerView mRecyclerView;
    @BindView(R.id.button_send_image)
    ImageButton button_send_image;
    @BindView(R.id.button_send_voice)
    ImageButton button_send_voice;
    @BindView(R.id.message)
    EditText mMessage;
    @BindView(R.id.button_send)
    Button mButtonSend;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.lin_color_background)
    LinearLayout lin_color_background;
    @BindView(R.id.menu_invite)
    ImageView chat_invite;
    @BindView(R.id.menu_member)
    ImageView chat_member;
    @BindView(R.id.menu_change_pass)
    ImageView chat_change_pass;
    @BindView(R.id.menu_change_emoticon)
    ImageView chat_change_emoticon;
    @BindView(R.id.menu_language)
    ImageView chat_language;
    @BindView(R.id.menu_leave)
    ImageView chat_leave;
    @BindView(R.id.lin_more_less)
    ConstraintLayout lin_more_less;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.chat_layout)
    DrawerLayout drawer;

    private static String mRef;
    private static String mRoom;
    private String mUsername;

    private EditText etInvite;
    private String mInvite = "";

    private Animation slide_down;
    private Animation slide_up;
    private boolean check_open_menu = true;

    private EditText etCurrentPass;
    private EditText etNewPass;
    private EditText etNewRepeat;
    private String mCurrentPass;
    private String mNewPass;
    private String mNewRepeat;

    private ChatAdapter chatAdapter;
    private ChatPresenter chatPresenter;
    private ArrayList<MessageModel> chat_data = new ArrayList<>();

    private String mEmoticon = "";

    private ProgressDialog mProgressDialog;
    private static final int OPEN_CAMERA = 1;
    private static final int GALLERY_PICK = 2;
    private static final int VIDEO_PICK = 3;

    private MediaRecorder mRecorder = null;
    private static String mFileName = null;

    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        //progressBar.setVisibility(View.INVISIBLE);
        chatAdapter = new ChatAdapter();
        chatPresenter = new ChatPresenter(this);

        mFileName = getExternalCacheDir().getAbsolutePath();
        mFileName += "/recorded_audio.m4a";

        setUpNavigationDrawer();
        setUpAnimationChatMenu();
        setUpCallPresenter();
        setUpOnClickTouch();
        setUpTextChange();

    }

    private void setUpNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        View headerView = navigationView.getHeaderView(0);
        TextView txt = (TextView) headerView.findViewById(R.id.txt);
        txt.setText(RealmManage.getInstance().getRealmData().get(0).getUsername());
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void setUpAnimationChatMenu() {
        lin_color_background = (LinearLayout) findViewById(R.id.lin_color_background);
        slide_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        lin_more_less.setVisibility(View.INVISIBLE);
        lin_color_background.setVisibility(View.INVISIBLE);
    }

    private void setUpCallPresenter() {
        Bundle bundle = getIntent().getExtras();
        mRef = bundle.getString("REF");
        mRoom = bundle.getString("ROOM");
        mEmoticon = bundle.getString("EMOTICON");
        mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();

        chatPresenter.queryData(mRef, mRoom, ChatActivity.this);
        chatPresenter.checkLeaveRoom(mRoom);
        chatPresenter.checkDeviceId();
        chatPresenter.checkEmoticon(mEmoticon);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUpOnClickTouch() {
        mButtonSend.setOnClickListener(this);
        chat_menu.setOnClickListener(this);
        lin_color_background.setOnClickListener(this);
        lin_more_less.setOnClickListener(this);
        chat_invite.setOnClickListener(this);
        chat_member.setOnClickListener(this);
        chat_change_pass.setOnClickListener(this);
        chat_change_emoticon.setOnClickListener(this);
        chat_language.setOnClickListener(this);
        chat_leave.setOnClickListener(this);
        button_send_image.setOnClickListener(this);
        mRecyclerView.setOnTouchListener(this);
        button_send_voice.setOnTouchListener(this);
    }

    private void setUpTextChange() {
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        mMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mButtonSend.setBackgroundResource(R.drawable.ic_send_black);
                mButtonSend.setText(null);
                params.weight = 0;
                button_send_image.setLayoutParams(params);
                button_send_voice.setLayoutParams(params);
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (mMessage.getText().toString().isEmpty()) {
                    mButtonSend.setBackgroundColor(Color.WHITE);
                    mButtonSend.setText(mEmoticon);
                    mButtonSend.setTextSize(20);
                    params.weight = (float) 0.15;
                    button_send_image.setLayoutParams(params);
                    button_send_voice.setLayoutParams(params);
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        chatPresenter.removeEvent();
        super.onDestroy();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_setting_room, menu);
//        return true;
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//
//            case R.id.setting_invite:
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
//                LayoutInflater inflater = getLayoutInflater();
//                View view = inflater.inflate(R.layout.template_invite_people,null);
//                mInvite = (EditText) view.findViewById(R.id.invite_room);
//
//                builder.setView(view)
//                        .setTitle("Password")
//                        .setNegativeButton("ไม่", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//
//                            }
//                        })
//                        .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//
//                                mId = mInvite.getText().toString();
//                                chatPresenter.invitePeople(mId);
//
//                            }
//                        })
//                        .create()
//                        .show();
//                break;
//            case R.id.setting_leave:
//
//                chatPresenter.leaveRoom(mRoom);
//                Toast.makeText(this, "LEAVE", Toast.LENGTH_SHORT).show();
//
//                break;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onClick(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
        LayoutInflater inflater = getLayoutInflater();

        switch (view.getId()) {
            case R.id.button_send:
                if (mMessage.getText().toString().isEmpty()) {
                    MessageModel message = new MessageModel();
                    message.setUsername(mUsername);
                    message.setMessage(mEmoticon);
                    message.setType_message("emoticon");
                    chatPresenter.sendMessage(message);

                } else {
                    MessageModel message = new MessageModel();
                    message.setUsername(mUsername);
                    message.setMessage(mMessage.getText().toString());
                    message.setType_message("text");
                    chatPresenter.sendMessage(message);
                }
                break;

            case R.id.button_send_image:

                final String open_camera = getResources().getString(R.string.open_camera);
                final String send_image = getResources().getString(R.string.send_image);
                final String send_video = getString(R.string.send_video);

                final CharSequence[] item = {open_camera, send_image, send_video};
                AlertDialog.Builder b = new AlertDialog.Builder(ChatActivity.this);
                b.setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (item[i].equals(open_camera)) {
                            chatPresenter.checkPermissionCamera();
                        } else if (item[i].equals(send_image)) {
                            chatPresenter.checkPermissionImage();
                        } else if (item[i].equals(send_video)) {
                            chatPresenter.checkPermissionVideo();
                        }
                    }
                });
                b.show();
                break;

            case R.id.chat_menu:
                if (check_open_menu) {
                    hideKeyboard(view);
                    lin_more_less.startAnimation(slide_down);
                    lin_more_less.setVisibility(View.VISIBLE);
                    lin_color_background.setVisibility(View.VISIBLE);
                    chat_menu.setImageResource(R.drawable.ic_expand_less);
                    check_open_menu = false;
                } else {
                    lin_color_background.setVisibility(View.INVISIBLE);
                    lin_more_less.startAnimation(slide_up);
                    lin_more_less.setVisibility(View.INVISIBLE);
                    chat_menu.setImageResource(R.drawable.ic_expand_more);
                    check_open_menu = true;
                }
                break;

            case R.id.lin_color_background:
                lin_color_background.setVisibility(View.INVISIBLE);
                lin_more_less.startAnimation(slide_up);
                lin_more_less.setVisibility(View.INVISIBLE);
                chat_menu.setImageResource(R.drawable.ic_expand_more);
                check_open_menu = true;
                break;

            case R.id.menu_invite:
                View invite = inflater.inflate(R.layout.template_invite_people, null);

                etInvite = (EditText) invite.findViewById(R.id.invite_room);

                builder.setView(invite)
                        .setTitle(R.string.invite)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                mInvite = etInvite.getText().toString();
                                chatPresenter.invitePeople(mInvite);

                            }
                        })
                        .create()
                        .show();
                break;

            case R.id.menu_member:

                Intent intent = new Intent(ChatActivity.this, MemberActivity.class);
                intent.putExtra("ROOM", mRoom);
                startActivity(intent);
                break;

            case R.id.menu_change_pass:
                View change_pass = inflater.inflate(R.layout.template_change_pass, null);

                etCurrentPass = (EditText) change_pass.findViewById(R.id.change_current);
                etNewPass = (EditText) change_pass.findViewById(R.id.change_new);
                etNewRepeat = (EditText) change_pass.findViewById(R.id.change_new_repeat);

                builder.setView(change_pass)
                        .setTitle(R.string.password)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                mCurrentPass = etCurrentPass.getText().toString();
                                mNewPass = etNewPass.getText().toString();
                                mNewRepeat = etNewRepeat.getText().toString();

                                chatPresenter.checkChangePass(mCurrentPass, mNewPass, mNewRepeat);
                            }
                        })
                        .create()
                        .show();

                break;

            case R.id.menu_change_emoticon:

                change_emoticon();

                break;
            case R.id.menu_language:
                final String thai = getString(R.string.thai);
                final String eng = getString(R.string.eng);
                final CharSequence[] item_language = {thai, eng};
                AlertDialog.Builder builder_language = new AlertDialog.Builder(ChatActivity.this);
                builder_language.setItems(item_language, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (item_language[i].equals(thai)) {
                            Intent lang = new Intent(ChatActivity.this, MainActivity.class);
                            lang.putExtra("LANGUAGE","th");
                            startActivity(lang);
                            finish();
                        } else if (item_language[i].equals(eng)) {
                            Intent lang = new Intent(ChatActivity.this, MainActivity.class);
                            lang.putExtra("LANGUAGE","en");
                            startActivity(lang);
                            finish();
                        }
                    }
                });
                builder_language.show();
                break;

            case R.id.menu_leave:
                new AlertDialog.Builder(ChatActivity.this)
                        .setTitle(R.string.want_leave_room)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                chatPresenter.leaveRoom(mRoom);
                            }
                        })
                        .create()
                        .show();
                break;
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        switch (view.getId()) {
            case R.id.rv_chat:
                hideKeyboard(view);
                break;
            case R.id.button_send_voice:
                chatPresenter.checkPermissionAudio(motionEvent);
                break;
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case OPEN_CAMERA:
                    chatPresenter.uploadImageFromCamera(uri);
                    break;
                case GALLERY_PICK:
                    String path = chatPresenter.getPath(this, Uri.parse(data.getData().toString()));
                    chatPresenter.uploadImage(path);
                    break;
                case VIDEO_PICK:
                    Toast.makeText(this, "VIDEO_PICK", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
        LayoutInflater inflater = getLayoutInflater();

        switch (menuItem.getItemId()) {
            case R.id.setting_invite:
                View invite = inflater.inflate(R.layout.template_invite_people, null);

                etInvite = (EditText) invite.findViewById(R.id.invite_room);

                builder.setView(invite)
                        .setTitle(R.string.invite)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                mInvite = etInvite.getText().toString();
                                chatPresenter.invitePeople(mInvite);

                            }
                        })
                        .create()
                        .show();
                break;

            case R.id.setting_member:

                Intent intent = new Intent(ChatActivity.this, MemberActivity.class);
                intent.putExtra("ROOM", mRoom);
                startActivity(intent);
                break;
            case R.id.setting_change_pass:
                View change_pass = inflater.inflate(R.layout.template_change_pass, null);

                etCurrentPass = (EditText) change_pass.findViewById(R.id.change_current);
                etNewPass = (EditText) change_pass.findViewById(R.id.change_new);
                etNewRepeat = (EditText) change_pass.findViewById(R.id.change_new_repeat);

                builder.setView(change_pass)
                        .setTitle(R.string.password)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                mCurrentPass = etCurrentPass.getText().toString();
                                mNewPass = etNewPass.getText().toString();
                                mNewRepeat = etNewRepeat.getText().toString();

                                chatPresenter.checkChangePass(mCurrentPass, mNewPass, mNewRepeat);
                            }
                        })
                        .create()
                        .show();

                break;
            case R.id.setting_leave:

                new AlertDialog.Builder(ChatActivity.this)
                        .setTitle(R.string.want_leave_room)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                chatPresenter.leaveRoom(mRoom);
                            }
                        })
                        .create()
                        .show();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.chat_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void getDataSuccess(ArrayList<MessageModel> data) {
        progressBar.setVisibility(View.VISIBLE);
        this.chat_data = data;

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        chatAdapter.setChatAdapter(ChatActivity.this, chat_data, mUsername, this);
        //linearLayoutManager.setReverseLayout(true);       //ทำให้ข้อความแสดงจากบนลงล่าง
        linearLayoutManager.setStackFromEnd(true);          //ทำให้เวลาเพิ่มข้อความ หน้าจอไม่เด้งขึ้นข้างบน แต่เด้งหน้าข้อความล่าสุดที่เพิ่ม
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(chatAdapter);
        chatAdapter.notifyDataSetChanged();

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void invitePeopleSuccess() {
        new AlertDialog.Builder(ChatActivity.this)
                .setMessage(R.string.invite_success)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void leaveRoomSuccess() {

        Intent intent = new Intent(ChatActivity.this, ManageActivity.class);
        startActivity(intent);
    }

    @Override
    public void checkCurrentPassFail() {
        new AlertDialog.Builder(ChatActivity.this)
                .setMessage(R.string.current_password_wrong)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void changeNewPassSuccess() {
        new AlertDialog.Builder(ChatActivity.this)
                .setMessage(R.string.change_password_success)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void changeNewPassFail() {
        new AlertDialog.Builder(ChatActivity.this)
                .setMessage(R.string.change_password_fail)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void setTitle(ArrayList<UserModel> data) {
        setTitle(mRoom + "(" + data.size() + ")");
    }

    @Override
    public void wasKickedOut() {
        AlertDialog b = new AlertDialog.Builder(ChatActivity.this)
                .setTitle(R.string.kicked_out_of_room)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .create();
        if (!isFinishing()) {
            b.show();
        }
    }

    @Override
    public void setText() {
        mMessage.setText("");
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void setEmoticon(String emoticon) {
        this.mEmoticon = emoticon;
        mButtonSend.setText(mEmoticon);
        mButtonSend.setTextSize(20);
    }

    private void change_emoticon() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View change_emoticon = inflater.inflate(R.layout.template_change_emoticon, null);

        TextView emoticon_slightly_smile = (TextView) change_emoticon.findViewById(R.id.emoticon_slightly_smile);
        TextView emoticon_smile_open_mouth = (TextView) change_emoticon.findViewById(R.id.emoticon_smile_open_mouth);
        TextView emoticon_heart_eyes = (TextView) change_emoticon.findViewById(R.id.emoticon_heart_eyes);
        TextView emoticon_pouting_face = (TextView) change_emoticon.findViewById(R.id.emoticon_pouting_face);
        TextView emoticon_red_heart = (TextView) change_emoticon.findViewById(R.id.emoticon_red_heart);
        TextView emoticon_middle_finger = (TextView) change_emoticon.findViewById(R.id.emoticon_middle_finger);

        builder.setView(change_emoticon)
                .create()
                .show();

        emoticon_slightly_smile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatPresenter.changeEmoticon(getString(R.string.emoticon_slightly_smile));
            }
        });
        emoticon_smile_open_mouth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatPresenter.changeEmoticon(getString(R.string.emoticon_smile_open_mouth));
            }
        });
        emoticon_heart_eyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatPresenter.changeEmoticon(getString(R.string.emoticon_heart_eyes));
            }
        });
        emoticon_pouting_face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatPresenter.changeEmoticon(getString(R.string.emoticon_pouting_face));
            }
        });
        emoticon_red_heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatPresenter.changeEmoticon(getString(R.string.emoticon_red_heart));
            }
        });
        emoticon_middle_finger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatPresenter.changeEmoticon(getString(R.string.emoticon_middle_finger));
            }
        });
    }

    @Override
    public void changeEmoticonSuccess() {
        new AlertDialog.Builder(ChatActivity.this)
                .setTitle(R.string.success)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void checkPermissionImageSuccess() {
        Intent pick_image = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pick_image, GALLERY_PICK);
    }

    @Override
    public void checkPermissionCameraSuccess() {
        String time = new SimpleDateFormat("yyyy-MM-dd_HH:MM:SS").format(new Date());
        String imageFileName = time + ".jpg";
        File imageFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String imagePath = imageFile.getAbsolutePath() + "/" + imageFileName;

        File file = new File(imagePath);
        uri = FileProvider.getUriForFile(ChatActivity.this, getApplicationContext().getPackageName() + ".provider", file);
        Intent open_camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        open_camera.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(open_camera, OPEN_CAMERA);
    }

    @Override
    public void initProgressDialog() {
        mProgressDialog = new ProgressDialog(ChatActivity.this);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                chatPresenter.cancelUpload();
            }
        });
        mProgressDialog.show();
    }

    @Override
    public void setLoading(int i) {
        mProgressDialog.setProgress(i);
    }

    @Override
    public void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void overSize() {

        new AlertDialog.Builder(ChatActivity.this)
                .setTitle(R.string.oversize5mb)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void startRecording() {

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e("FAIL", "prepare() failed");
        }
        mRecorder.start();
    }

    @Override
    public void stopRecording() {
        try {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
            chatPresenter.uploadAudio(mFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void checkPermissionVideoSuccess() {
        Log.e("open","Video");
        Intent pick_video = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        pick_video.setType("video/*");
        startActivityForResult(pick_video, VIDEO_PICK);
    }

    @Override
    public void sendKeyMessage(final String key) {
        new AlertDialog.Builder(ChatActivity.this)
                .setTitle(R.string.want_delete)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        chatPresenter.delMessage(key);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void playAudio(String url) {
        Toast.makeText(this, "Play", Toast.LENGTH_SHORT).show();
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                }
            });
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendUrl(final String key, final String url) {
        new AlertDialog.Builder(ChatActivity.this)
                .setTitle(R.string.want_delete)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        chatPresenter.deleteFileInStorage(key, url);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void setZoomImage(String url) {
        Intent zoomImage = new Intent(ChatActivity.this, ImageActivity.class);
        zoomImage.putExtra("URL", url);
        zoomImage.putExtra("ROOM", mRoom);
        //zoomImage.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(zoomImage);
        this.overridePendingTransition(0, 0);
    }


    public void hideKeyboard(View view) {
        mMessage.clearFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
