package com.example.petch.testfirebase.module.chat;


import android.content.Context;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.view.MotionEvent;

import com.example.petch.testfirebase.model.MessageModel;
import com.example.petch.testfirebase.model.UserModel;

import java.util.ArrayList;

public class ChatConstructor {

    public interface ChatSetPresenter{

        void queryData(String mRef,String mRoom,Context context);

        void sendMessage(MessageModel message);

        void invitePeople(String id);

        void checkLeaveRoom(String room);

        void leaveRoom(String room);

        void checkChangePass(String check_current,String new_pass,String repeat_pass);

        void checkDeviceId();

        void checkEmoticon(String emoticon);

        void changeEmoticon(String emoticon);

        void checkPermissionImage();

        String getPath(Context context, Uri uri);

        void uploadImage(String path);

        void checkPermissionCamera();

        void uploadImageFromCamera(Uri path);

        void cancelUpload();

        void checkPermissionAudio(MotionEvent motionEvent);

        void uploadAudio(String mFilename);

        void delMessage(String key);

        void deleteFileInStorage(String key,String url);

        void removeEvent();

        void checkPermissionVideo();
    }

    public interface View{

        void getDataSuccess(ArrayList<MessageModel> data);

        void invitePeopleSuccess();

        void leaveRoomSuccess();

        void checkCurrentPassFail();

        void changeNewPassSuccess();

        void changeNewPassFail();

        void setTitle(ArrayList<UserModel> data);

        void wasKickedOut();

        void setText();

        void showLoading();

        void setEmoticon(String emoticon);

        void changeEmoticonSuccess();

        void checkPermissionImageSuccess();

        void checkPermissionCameraSuccess();

        void initProgressDialog();

        void setLoading(int i);

        void dismissProgressDialog();

        void overSize();

        void startRecording();

        void stopRecording();

        void checkPermissionVideoSuccess();
    }

    public interface DelChatMessage {

        void sendKeyMessage(String key);

        void playAudio(String url);

        void sendUrl(String key,String url);

        void setZoomImage(String url);
    }

}
