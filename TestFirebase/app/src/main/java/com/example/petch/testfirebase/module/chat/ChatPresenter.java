package com.example.petch.testfirebase.module.chat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.MessageModel;
import com.example.petch.testfirebase.model.UserModel;
import com.example.petch.testfirebase.realm.RealmManage;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.HttpHeaders;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ChatPresenter implements ChatConstructor.ChatSetPresenter{

    private ChatConstructor.View view;
    private DatabaseReference databaseReference;
    private DatabaseReference mDataChat;
    private DatabaseReference mLeaveData;
    private ArrayList<MessageModel> chat_queryData = new ArrayList<>();
    private String mRef;
    private String mRoom;
    private String current_password = "";
    private ChildEventListener childEventListener;
    private boolean checkRemove = true;

    private ArrayList<UserModel> chat_userData = new ArrayList<>();

    //
    private JSONArray registration_ids = new JSONArray();
    private Context mContext;
    private String deviceId = "";
    private String mUsername = "";
    private Boolean checkUser = true;
    //

    private UploadTask mUploadTask;

    ChatPresenter(ChatConstructor.View view) {
        this.view = view;
    }

    @Override
    public void queryData(String mRef,String mRoom,Context context) {
        this.mRef = mRef;
        this.mRoom = mRoom;
        this.mContext = context;

        databaseReference = FirebaseDatabase.getInstance().getReference(mRef).child(mRoom);
        mDataChat = databaseReference.child("Messages");

        mDataChat.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                view.showLoading();
                String key = dataSnapshot.getKey();

                Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();
                String username = newPost.get("user").toString();
                String message = newPost.get("message").toString();
                String type_message = "";
                String time = newPost.get("time").toString();
                String date = newPost.get("date").toString();

                try{
                    type_message = newPost.get("type_message").toString();
                }catch (Exception e){
                    e.printStackTrace();
                }

                //ChatItem data = new ChatItem();
                MessageModel data = new MessageModel();
                data.setKey(key);
                data.setUsername(username);
                data.setMessage(message);
                data.setType_message(type_message);
                data.setTime(time);
                data.setDate(date);
                chat_queryData.add(data);

                view.getDataSuccess(chat_queryData);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                String delRoom = dataSnapshot.getKey();

                for(int i = 0; i < chat_queryData.size(); i++){

                    if(delRoom.equals(chat_queryData.get(i).getKey())){

                        chat_queryData.remove(i);
                    }
                }
                view.getDataSuccess(chat_queryData);

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    @Override
    public void sendMessage(MessageModel message) {

        ///
        //this.mContext = mContext;

        try {
            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
        }catch (Exception e){
            e.printStackTrace();
        }
        ///


        Date currentDateTime = Calendar.getInstance().getTime();
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        String time = formatTime.format(currentDateTime);

        final Calendar c = Calendar.getInstance();
        int  mYear = c.get(Calendar.YEAR) + 543;
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String date = mDay+"/"+mMonth+"/"+ mYear;

        Map<String, Object> chat = new HashMap<String, Object>();
        chat.put("user", message.getUsername());
        chat.put("message",message.getMessage());
        chat.put("type_message",message.getType_message());
        chat.put("time",time);
        chat.put("date",date);
        chat.put("deviceId",deviceId);

        mDataChat.push().setValue(chat);
        view.setText();


        ////
        if (registration_ids.length() > 0) {

            String url = "https://fcm.googleapis.com/fcm/send";
            AsyncHttpClient client = new AsyncHttpClient();

            client.addHeader(HttpHeaders.AUTHORIZATION, "key=AIzaSyBE_svX7kJjOHXRhRKu1tSceiMDQyq7NRg");
            client.addHeader(HttpHeaders.CONTENT_TYPE, RequestParams.APPLICATION_JSON);

            try {

                JSONObject params = new JSONObject();
                params.put("registration_ids", registration_ids);

                JSONObject notificationObject = new JSONObject();
                notificationObject.put("body", message.getMessage());
                notificationObject.put("title", message.getUsername());
                notificationObject.put("test","test");
                params.put("notification", notificationObject);

                StringEntity entity = new StringEntity(params.toString(),"UTF-8");
                client.post(mContext, url, entity, RequestParams.APPLICATION_JSON, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {

                    }
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ///////////////////////

    }

    @Override
    public void invitePeople(String id) {

        databaseReference.child("User").child(id).setValue("");
        view.invitePeopleSuccess();
        Log.e("INVITE",id);

    }

    @Override
    public void checkLeaveRoom(String room) {
        mLeaveData = databaseReference.child("User");

        childEventListener = mLeaveData.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    try {
                        String username = dataSnapshot.getKey();
                        UserModel data = new UserModel();
                        data.setUsername(username);
                        chat_userData.add(data);
                        view.setTitle(chat_userData);

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    //count++;
                    //Log.e("COUNT",String.valueOf(count));
                    Log.e("COUNT", "SIZE "+ String.valueOf(chat_userData.size()));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                String user = dataSnapshot.getKey();
                Log.e("REMOVE_Chat",user + " = " + RealmManage.getInstance().getRealmData().get(0).getUsername());

                for(int i = 0; i < chat_userData.size(); i++){
                    if(user.contentEquals(chat_userData.get(i).getUsername())){
                        chat_userData.remove(i);
                        view.setTitle(chat_userData);
                    }
                }

                if(user.contentEquals(RealmManage.getInstance().getRealmData().get(0).getUsername())){
                    if(checkRemove) {
                        view.wasKickedOut();
                    }else {
                        checkRemove = true;
                    }
                }
            }
            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void leaveRoom(String room) {
        checkRemove = false;
        String username = RealmManage.getInstance().getRealmData().get(0).getUsername();
        if(chat_userData.size() == 1){

            databaseReference.removeValue();
            view.leaveRoomSuccess();

        }else if(chat_userData.size() > 1) {

            databaseReference.child("User").child(username).removeValue();
            view.leaveRoomSuccess();
        }
    }

    @Override
    public void checkChangePass(String check_current, String new_pass, String repeat_pass) {
        checkPassword();

        databaseReference = FirebaseDatabase.getInstance().getReference(mRef).child(mRoom);

        if(current_password.isEmpty()){

            if(new_pass.equals(repeat_pass)){

                databaseReference.child("Password").setValue(new_pass);
                view.changeNewPassSuccess();

            }else {
                view.changeNewPassFail();
            }

        }else if(current_password.contentEquals(check_current)){

            if(new_pass.equals(repeat_pass)){

                databaseReference.child("Password").setValue(new_pass);
                view.changeNewPassSuccess();

            }else {
                view.changeNewPassFail();
            }
        }else{

            view.checkCurrentPassFail();

        }
    }

    private void checkPassword(){

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Room");
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                try {
                    Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();
                    if (newPost != null) {
                        if(mRoom.contentEquals(dataSnapshot.getKey())){
                            current_password = newPost.get("Password").toString();
                        }
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                try {
                    Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();
                    if (newPost != null) {
                        if(mRoom.contentEquals(dataSnapshot.getKey())){
                            current_password = newPost.get("Password").toString();
                        }
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("PASSWORD",current_password);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void checkDeviceId() {

        //
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersRef = reference.child("User");
        try {
            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
        }catch (Exception e){
            e.printStackTrace();
        }
        //

        final ValueEventListener userValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                registration_ids = new JSONArray();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    try {

                        Map<String, Object> newPost = (Map<String, Object>) postSnapshot.getValue();
                        String username = newPost.get("Username").toString();
                        String deviceIds = newPost.get("deviceId").toString();
                        String deviceToken = newPost.get("deviceToken").toString();

                        for(int i = 0; i<chat_userData.size(); i++) {
                            if (username.contentEquals(chat_userData.get(i).getUsername())){
                                checkUser = true;
                            }
                        }

                        if(checkUser) {
                            if (!deviceIds.contentEquals(deviceId) && !deviceToken.isEmpty()) {
                                registration_ids.put(deviceToken);
                            }
                            checkUser = false;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                registration_ids = new JSONArray();
            }
        };
        usersRef.addValueEventListener(userValueEventListener);

    }

    @Override
    public void checkEmoticon(String emoticon) {

        if(!emoticon.isEmpty()) {

            if (emoticon.contentEquals(mContext.getString(R.string.emoticon_slightly_smile))) {

                view.setEmoticon(emoticon);

            } else if (emoticon.contentEquals(mContext.getString(R.string.emoticon_smile_open_mouth))) {

                view.setEmoticon(emoticon);

            } else if (emoticon.contentEquals(mContext.getString(R.string.emoticon_heart_eyes))) {

                view.setEmoticon(emoticon);

            } else if (emoticon.contentEquals(mContext.getString(R.string.emoticon_pouting_face))) {

                view.setEmoticon(emoticon);

            } else if (emoticon.contentEquals(mContext.getString(R.string.emoticon_red_heart))) {

                view.setEmoticon(emoticon);

            } else if (emoticon.contentEquals(mContext.getString(R.string.emoticon_middle_finger))) {

                view.setEmoticon(emoticon);
            }
        }
    }

    @Override
    public void changeEmoticon(String emoticon) {

        databaseReference.child("Emoticon").setValue(emoticon);
        checkEmoticon(emoticon);
        view.changeEmoticonSuccess();

    }

    @Override
    public void checkPermissionImage() {

        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            view.checkPermissionImageSuccess();

        } else{
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }
    }

    @Override
    public String getPath(Context context, Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    @Override
    public void uploadImage(String path) {

        Uri file = Uri.fromFile(new File(path));
        int size_file = Integer.parseInt(String.valueOf(new File(path).length() / 1024));
        if (size_file < 5120) {

            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            final StorageReference folderRef = storageRef.child("message").child("image").child(file.getLastPathSegment());
            mUploadTask = folderRef.putFile(file);

            view.initProgressDialog();

            mUploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                    view.setLoading(progress);
                }
            }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();

                    }
                    return folderRef.getDownloadUrl();
                }
            }).addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    view.dismissProgressDialog();
                    uploadImageSuccess(uri.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    view.dismissProgressDialog();
                    Toast.makeText(mContext, String.format("Failure: %s", e.getMessage()), Toast.LENGTH_SHORT).show();
                }
            });
        } else {

            view.overSize();
        }
    }

    @Override
    public void checkPermissionCamera() {
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
        {
            view.checkPermissionCameraSuccess();

        } else{
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.CAMERA}, 1);
        }
    }

    @Override
    public void uploadImageFromCamera(Uri path) {
        int dataSize = 0;
        try {
            InputStream fileInputStream = mContext.getContentResolver().openInputStream(path);
            dataSize = fileInputStream.available() / 1024;
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (dataSize < 5120) {
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            final StorageReference folderRef = storageRef.child("message").child("image").child(path.getLastPathSegment());
            mUploadTask = folderRef.putFile(path);

            view.initProgressDialog();

            mUploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                    view.setLoading(progress);
                }
            }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();

                    }
                    return folderRef.getDownloadUrl();
                }
            }).addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    view.dismissProgressDialog();
                    uploadImageSuccess(uri.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    view.dismissProgressDialog();
                    Toast.makeText(mContext, String.format("Failure: %s", e.getMessage()), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            view.overSize();
        }
    }

    private void uploadImageSuccess(String url) {
        try {

            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
            mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();
        }catch (Exception e){
            e.printStackTrace();
        }

        Date currentDateTime = Calendar.getInstance().getTime();
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        final String time = formatTime.format(currentDateTime);

        final Calendar c = Calendar.getInstance();
        int  mYear = c.get(Calendar.YEAR) + 543;
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        final String date = mDay+"/"+mMonth+"/"+ mYear;

        Map<String, Object> chat = new HashMap<String, Object>();
        chat.put("user", mUsername);
        chat.put("message",url);
        chat.put("type_message","image");
        chat.put("time",time);
        chat.put("date",date);
        chat.put("deviceId",deviceId);
        mDataChat.push().setValue(chat);
    }

    @Override
    public void checkPermissionAudio(MotionEvent motionEvent) {

        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
        {
            if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                view.startRecording();
            }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                view.stopRecording();
            }

        } else{
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO}, 111);
        }
    }

    @Override
    public void uploadAudio(String mFilename) {

        Uri uri = Uri.fromFile(new File(mFilename));
        String time = new SimpleDateFormat("yyyy-MM-dd_HH:MM:SS").format(new Date());
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        final StorageReference audio = storageRef.child("message").child("audio").child(time +".m4a");
        mUploadTask = audio.putFile(uri);

        view.initProgressDialog();

        mUploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                view.setLoading(progress);
            }
        }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return audio.getDownloadUrl();
            }
        }).addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                view.dismissProgressDialog();
                uploadAudioSuccess(uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                view.dismissProgressDialog();
                Toast.makeText(mContext, String.format("Failure: %s", e.getMessage()), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void uploadAudioSuccess(String url){
        try {

            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
            mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();
        }catch (Exception e){
            e.printStackTrace();
        }

        Date currentDateTime = Calendar.getInstance().getTime();
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        final String time = formatTime.format(currentDateTime);

        final Calendar c = Calendar.getInstance();
        int  mYear = c.get(Calendar.YEAR) + 543;
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        final String date = mDay+"/"+mMonth+"/"+ mYear;

        Map<String, Object> chat = new HashMap<String, Object>();
        chat.put("user", mUsername);
        chat.put("message",url);
        chat.put("type_message","audio");
        chat.put("time",time);
        chat.put("date",date);
        chat.put("deviceId",deviceId);
        mDataChat.push().setValue(chat);

    }

    @Override
    public void cancelUpload() {
        mUploadTask.cancel();
    }

    @Override
    public void delMessage(String key) {
        mDataChat.child(key).removeValue();
    }

    @Override
    public void deleteFileInStorage(String key,String url) {

        mDataChat.child(key).removeValue();
        FirebaseStorage mFirebaseStorage = FirebaseStorage.getInstance();
        StorageReference photoRef = mFirebaseStorage.getReferenceFromUrl(url);
        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(mContext, "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void removeEvent() {
        if(childEventListener != null){
            mLeaveData.removeEventListener(childEventListener);
        }
    }

    @Override
    public void checkPermissionVideo() {
        if(ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            view.checkPermissionVideoSuccess();
        } else {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
        }
    }


}
