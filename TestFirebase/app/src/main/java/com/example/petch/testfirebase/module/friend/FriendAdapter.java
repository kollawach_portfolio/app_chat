package com.example.petch.testfirebase.module.friend;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.module.chat.ChatActivity;

import java.util.ArrayList;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.viewHolder> {

    private Context context;
    private ArrayList<FriendItem> friendItems;

    public void setFriendAdapter(Context context, ArrayList<FriendItem> friendItems){
        this.context = context;
        this.friendItems = friendItems;
    }

    @NonNull
    @Override
    public FriendAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_friend, viewGroup,false);
        return new FriendAdapter.viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendAdapter.viewHolder viewHolder, final int position) {

        final String friend = friendItems.get(position).getFriend();
        viewHolder.txtFriend.setText(friend);

        viewHolder.iv_openFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("NAME", friend);
                intent.putExtra("REF","Friend");
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if(friendItems == null)
            return 0;
        if (friendItems.isEmpty())
            return 0;
        return friendItems.size();
    }


    static class viewHolder extends RecyclerView.ViewHolder {
        TextView txtFriend;
        ImageView iv_openFriend;


        public viewHolder(View itemView) {
            super(itemView);
            txtFriend = itemView.findViewById(R.id.txt_friend);
            iv_openFriend = itemView.findViewById(R.id.openFriend);
        }
    }
}
