package com.example.petch.testfirebase.module.friend;

public class FriendConstructor {

    public interface  FriendSetPresenter{
        void queryData();

    }

    public interface View {

        void getDataSuccess(FriendItem friendItem);
    }

}
