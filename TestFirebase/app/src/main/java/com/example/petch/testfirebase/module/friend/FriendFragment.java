package com.example.petch.testfirebase.module.friend;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.petch.testfirebase.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FriendFragment extends Fragment implements FriendConstructor.View{

    @BindView(R.id.rv_friend)
    RecyclerView rvFriend;
    Unbinder unbinder;

    private FriendAdapter friendAdapter;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<FriendItem> friendItems = new ArrayList<>();
    private FriendPresenter friendPresenter;



    public static FriendFragment newInstance() {
        return new FriendFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        friendAdapter = new FriendAdapter();
        friendPresenter = new FriendPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        friendPresenter.queryData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //unbinder.unbind();
        friendItems.clear();
    }


    @Override
    public void getDataSuccess(FriendItem friendItem) {

        friendItems.add(friendItem);

        linearLayoutManager = new LinearLayoutManager(getContext());
        friendAdapter.setFriendAdapter(getContext(), friendItems);
        rvFriend.setLayoutManager(linearLayoutManager);
        rvFriend.setAdapter(friendAdapter);
        friendAdapter.notifyDataSetChanged();
    }
}
