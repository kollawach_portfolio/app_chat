package com.example.petch.testfirebase.module.friend;


public class FriendItem{


        private String friend;
        private String username;

    public String getFriend() {
        return friend;
    }

    public void setFriend(String friend) {
        this.friend = friend;
    }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

}
