package com.example.petch.testfirebase.module.friend;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class FriendPresenter implements FriendConstructor.FriendSetPresenter {

    private DatabaseReference databaseReference;
    private DatabaseReference mDataFriend;
    private FriendConstructor.View view;

    public FriendPresenter(FriendConstructor.View view) {
        this.view = view;
    }

    @Override
    public void queryData() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mDataFriend = databaseReference.child("Friend");

            Query query = mDataFriend;
            query.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    String friendKey = dataSnapshot.getKey();

                    FriendItem friendItem = new FriendItem();
                    friendItem.setFriend(friendKey);
                    view.getDataSuccess(friendItem);


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });
    }
}
