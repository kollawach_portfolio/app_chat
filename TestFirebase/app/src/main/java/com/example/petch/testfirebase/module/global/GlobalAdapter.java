package com.example.petch.testfirebase.module.global;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.MessageModel;

import java.util.ArrayList;


public class GlobalAdapter extends RecyclerView.Adapter<GlobalAdapter.viewHolder> {

    private String username;
    private Context context;
    private ArrayList<MessageModel> global = new ArrayList<>();
    private GlobalConstructor.DelMessage delMessage;

    public void setGlobalAdapter(Context context, ArrayList<MessageModel> data,String username,GlobalConstructor.DelMessage delMessage){
        this.context = context;
        this.global = data;
        this.username = username;
        this.delMessage = delMessage;
    }

    @NonNull
    @Override
    public GlobalAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_chat, viewGroup,false);
        return new GlobalAdapter.viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GlobalAdapter.viewHolder viewHolder, int position) {

        final String key = global.get(position).getKey();
        String user = global.get(position).getUsername();
        final String message = global.get(position).getMessage();
        String type = global.get(position).getType_message();
        String time = global.get(position).getTime();
        String date = global.get(position).getDate();

        String previousDate = "";

        if(position>0){
            previousDate = global.get(position-1).getDate();
        }

        if(previousDate.isEmpty()){
            viewHolder.txtChatDate.setVisibility(View.VISIBLE);
            viewHolder.txtChatDate.setText(date);

        }else {
            if(previousDate.contentEquals(date)){
                viewHolder.txtChatDate.setVisibility(View.GONE);
                viewHolder.txtChatDate.setText("");
            }else {
                viewHolder.txtChatDate.setVisibility(View.VISIBLE);
                viewHolder.txtChatDate.setText(date);
            }
        }


        //ME
        if (username.contentEquals(user)) {

            viewHolder.relativeLayout.setPadding(180,0,0,0);
            //viewHolder.txtMyMessage.setVisibility(View.VISIBLE);
            viewHolder.txtTimeMyMessage.setVisibility(View.VISIBLE);

            viewHolder.txtUser.setText("");

            if(type.contentEquals("image")){

                viewHolder.my_message_image.setVisibility(View.VISIBLE);
                viewHolder.time_my_message_image.setVisibility(View.VISIBLE);
                Glide.with(context).load(message).into(viewHolder.my_message_image);
                viewHolder.time_my_message_image.setText(time);

                viewHolder.txtMyMessage.setVisibility(View.GONE);
                viewHolder.txtTimeMyMessage.setVisibility(View.GONE);
                viewHolder.my_message_voice .setVisibility(View.GONE);
                viewHolder.time_my_message_voice.setVisibility(View.GONE);

            }else if (type.contentEquals("emoticon")) {

                viewHolder.txtMyMessage.setVisibility(View.VISIBLE);
                viewHolder.txtTimeMyMessage.setVisibility(View.VISIBLE);
                viewHolder.txtMyMessage.setText(message);
                viewHolder.txtMyMessage.setBackground(null);
                viewHolder.txtTimeMyMessage.setText(time);

                viewHolder.my_message_image.setVisibility(View.GONE);
                viewHolder.time_my_message_image.setVisibility(View.GONE);
                viewHolder.my_message_voice .setVisibility(View.GONE);
                viewHolder.time_my_message_voice.setVisibility(View.GONE);

            }else if(type.contentEquals("audio")){

                viewHolder.my_message_voice .setVisibility(View.VISIBLE);
                viewHolder.time_my_message_voice.setVisibility(View.VISIBLE);

                viewHolder.my_message_voice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        delMessage.playAudio(message);
                    }
                });
                viewHolder.time_my_message_voice.setText(time);

                viewHolder.txtMyMessage.setVisibility(View.GONE);
                viewHolder.txtTimeMyMessage.setVisibility(View.GONE);
                viewHolder.my_message_image.setVisibility(View.GONE);
                viewHolder.time_my_message_image.setVisibility(View.GONE);
            }else{

                viewHolder.txtMyMessage.setVisibility(View.VISIBLE);
                viewHolder.txtTimeMyMessage.setVisibility(View.VISIBLE);
                viewHolder.txtMyMessage.setText(message);
                viewHolder.txtTimeMyMessage.setText(time);

                viewHolder.my_message_image.setVisibility(View.GONE);
                viewHolder.time_my_message_image.setVisibility(View.GONE);
                viewHolder.my_message_voice .setVisibility(View.GONE);
                viewHolder.time_my_message_voice.setVisibility(View.GONE);
            }



            viewHolder.txtTheirMessage.setVisibility(View.GONE);
            viewHolder.txtTimeTheirMessage.setVisibility(View.GONE);
            viewHolder.their_message_image.setVisibility(View.GONE);
            viewHolder.time_their_message_image.setVisibility(View.GONE);
            viewHolder.their_message_voice.setVisibility(View.GONE);
            viewHolder.time_their_message_voice.setVisibility(View.GONE);
            viewHolder.their_profile.setVisibility(View.GONE);

            //THEIR
        } else if(!(username.contentEquals(user))) {

            viewHolder.their_profile.setVisibility(View.VISIBLE);
            viewHolder.txtTheirMessage.setVisibility(View.VISIBLE);
            //viewHolder.txtTimeTheirMessage.setVisibility(View.VISIBLE);
            viewHolder.relativeLayout.setPadding(0,0,180,0);

            viewHolder.txtUser.setText(user + " :");


            if(type.contentEquals("image")){
                viewHolder.their_message_image.setVisibility(View.VISIBLE);
                viewHolder.time_their_message_image.setVisibility(View.VISIBLE);
                Glide.with(context).load(message).into(viewHolder.their_message_image);
                viewHolder.time_their_message_image.setText(time);


                viewHolder.txtTheirMessage.setVisibility(View.GONE);
                viewHolder.txtTimeTheirMessage.setVisibility(View.GONE);
                viewHolder.their_message_voice.setVisibility(View.GONE);
                viewHolder.time_their_message_voice.setVisibility(View.GONE);

            }else if (type.contentEquals("emoticon")) {

                viewHolder.txtTimeTheirMessage.setVisibility(View.VISIBLE);
                viewHolder.txtTimeTheirMessage.setVisibility(View.VISIBLE);
                viewHolder.txtTheirMessage.setText(message);
                viewHolder.txtTheirMessage.setBackground(null);
                viewHolder.txtTimeTheirMessage.setText(time);

                viewHolder.their_message_image.setVisibility(View.GONE);
                viewHolder.time_their_message_image.setVisibility(View.GONE);
                viewHolder.their_message_voice.setVisibility(View.GONE);
                viewHolder.time_their_message_voice.setVisibility(View.GONE);

            }else if(type.contentEquals("audio")){

                viewHolder.their_message_voice.setVisibility(View.VISIBLE);
                viewHolder.time_their_message_voice.setVisibility(View.VISIBLE);

                viewHolder.their_message_voice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        delMessage.playAudio(message);
                    }
                });
                viewHolder.time_their_message_voice.setText(time);

                viewHolder.txtTheirMessage.setVisibility(View.GONE);
                viewHolder.txtTimeTheirMessage.setVisibility(View.GONE);
                viewHolder.their_message_image.setVisibility(View.GONE);
                viewHolder.time_their_message_image.setVisibility(View.GONE);

            }else {


                viewHolder.txtTimeTheirMessage.setVisibility(View.VISIBLE);
                viewHolder.txtTimeTheirMessage.setVisibility(View.VISIBLE);
                viewHolder.txtTheirMessage.setText(message);
                viewHolder.txtTimeTheirMessage.setText(time);


                viewHolder.their_message_image.setVisibility(View.GONE);
                viewHolder.time_their_message_image.setVisibility(View.GONE);
                viewHolder.their_message_voice.setVisibility(View.GONE);
                viewHolder.time_their_message_voice.setVisibility(View.GONE);
            }

            viewHolder.txtMyMessage.setVisibility(View.GONE);
            viewHolder.txtTimeMyMessage.setVisibility(View.GONE);
            viewHolder.my_message_image.setVisibility(View.GONE);
            viewHolder.time_my_message_image.setVisibility(View.GONE);
            viewHolder.my_message_voice .setVisibility(View.GONE);
            viewHolder.time_my_message_voice.setVisibility(View.GONE);
        }


        final PopupMenu popupMenu = new PopupMenu(context,viewHolder.txtMyMessage);
        popupMenu.getMenuInflater().inflate(R.menu.menu_delete,popupMenu.getMenu());
        //เมื่อกดจะทำแสดง popupMenu ออกมา
        viewHolder.txtMyMessage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        delMessage.sendKeyMessage(key);
                        return true;
                    }
                });
                popupMenu.show();
                return false;
            }
        });


        viewHolder.my_message_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final CharSequence[] item = {"Delete","Download"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(item[i].equals("Delete")){
                            delMessage.sendUrl(key,message);
                        }else if(item[i].equals("Download")){
                            Intent download = new Intent(Intent.ACTION_VIEW);
                            download.setData(Uri.parse(message));
                            context.startActivity(download);
                        }
                    }
                });
                builder.show();
                return false;
            }
        });


        viewHolder.my_message_voice.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final CharSequence[] item = {"Delete"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(item[i].equals("Delete")){
                            delMessage.sendUrl(key,message);
                        }
                    }
                });
                builder.show();
                return false;
            }
        });


        viewHolder.my_message_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delMessage.setZoomImage(message);
            }
        });
        viewHolder.their_message_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delMessage.setZoomImage(message);
            }
        });


    }

    @Override
    public int getItemCount() {
        if(global == null)
            return 0;
        if(global.isEmpty())
            return 0;
        return global.size();
    }

    static class viewHolder extends RecyclerView.ViewHolder {
        TextView txtUser;
        TextView txtMyMessage;
        TextView txtTheirMessage;
        TextView txtTimeMyMessage;
        TextView txtTimeTheirMessage;
        TextView txtChatDate;
        RelativeLayout relativeLayout;

        ImageView my_message_image;
        ImageView their_message_image;
        TextView time_my_message_image;
        TextView time_their_message_image;

        Button my_message_voice;
        Button their_message_voice;
        TextView time_my_message_voice;
        TextView time_their_message_voice;

        ImageView their_profile;

        viewHolder(View itemView) {
            super(itemView);
            txtUser = itemView.findViewById(R.id.user_message);
            txtMyMessage = itemView.findViewById(R.id.my_message);
            txtTheirMessage = itemView.findViewById(R.id.their_message);
            txtTimeMyMessage = itemView.findViewById(R.id.time_my_message);
            txtTimeTheirMessage = itemView.findViewById(R.id.time_their_message);
            txtChatDate = itemView.findViewById(R.id.chat_date);
            relativeLayout = itemView.findViewById(R.id.rl_chat);

            my_message_image = itemView.findViewById(R.id.my_message_image);
            their_message_image  = itemView.findViewById(R.id.their_message_image);
            time_my_message_image = itemView.findViewById(R.id.time_my_message_image);
            time_their_message_image = itemView.findViewById(R.id.time_their_message_image);


            my_message_voice = itemView.findViewById(R.id.my_message_voice);
            their_message_voice = itemView.findViewById(R.id.their_message_voice);
            time_my_message_voice = itemView.findViewById(R.id.time_my_message_voice);
            time_their_message_voice = itemView.findViewById(R.id.time_their_message_voice);


            their_profile = itemView.findViewById(R.id.their_profile);
        }
    }
}

