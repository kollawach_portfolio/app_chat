package com.example.petch.testfirebase.module.global;

import android.content.Context;
import android.net.Uri;
import android.view.MotionEvent;

import com.example.petch.testfirebase.model.MessageModel;
import com.google.firebase.database.ChildEventListener;

import java.util.ArrayList;

public class GlobalConstructor {

    public interface GlobalSetPresenter{

        void queryData(Context context);

        void sendMessage(MessageModel message);

        void checkDeviceId();

        void checkPermissionImage();

        String getPath(Context context, Uri uri);

        void uploadImage(String path);

        void checkPermissionCamera();

        void uploadImageFromCamera(Uri path);

        void cancelUpload();

        void checkPermissionAudio(MotionEvent motionEvent);

        void uploadAudio(String mFilename);

        void delMessage(String key);

        void deleteFileInStorage(String key,String url);

    }

    public interface View{

        void getDataSuccess(ArrayList<MessageModel> data, ChildEventListener childEventListener);

        void setText();

        void showLoading();

        void checkPermissionImageSuccess();

        void checkPermissionCameraSuccess();


        void initProgressDialog();

        void setLoading(int i);

        void dismissProgressDialog();

        void overSize();

        void startRecording();

        void stopRecording();

    }

    public interface DelMessage {

        void sendKeyMessage(String key);

        void playAudio(String url);

        void sendUrl(String key,String url);

        void setZoomImage(String url);
    }
}
