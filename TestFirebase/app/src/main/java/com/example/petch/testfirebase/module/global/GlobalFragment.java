package com.example.petch.testfirebase.module.global;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.MessageModel;
import com.example.petch.testfirebase.module.chat.ChatActivity;
import com.example.petch.testfirebase.module.image.ImageActivity;
import com.example.petch.testfirebase.realm.RealmManage;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

public class GlobalFragment extends Fragment implements GlobalConstructor.View, GlobalConstructor.DelMessage,View.OnClickListener,View.OnTouchListener {

    @BindView(R.id.rv_global)
    RecyclerView rvGlobal;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.button_send)
    Button buttonSend;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    Unbinder unbinder;
    @BindView(R.id.button_send_image)
    ImageButton buttonSendImage;
    @BindView(R.id.button_send_voice)
    ImageButton buttonSendVoice;

    private GlobalAdapter globalAdapter;
    private String mUsername = "";
    private ArrayList<MessageModel> global_data = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private GlobalPresenter globalPresenter;

    private DatabaseReference databaseReference;
    private DatabaseReference mGlobal;
    private ChildEventListener childEventListener;



    private ProgressDialog mProgressDialog;
    private static final int OPEN_CAMERA = 1;
    private static final int GALLERY_PICK = 2;

    private MediaRecorder mRecorder = null;
    private static String mFileName = null;

    private Uri uri;


    public static GlobalFragment newInstance() {
        return new GlobalFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        globalAdapter = new GlobalAdapter();
        globalPresenter = new GlobalPresenter(this);

        try {
            mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();
        } catch (Exception e) {
            e.printStackTrace();
        }

        databaseReference = FirebaseDatabase.getInstance().getReference("Global");
        mGlobal = databaseReference.child("Messages");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_global, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpTextChange();
        setUpPresenter();
        setUpOnClickTouch();

        mFileName = getActivity().getExternalCacheDir().getAbsolutePath();
        mFileName += "/recorded_audio.3gp";

    }

    private void setUpTextChange(){
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                params.weight = 0;
                buttonSendImage.setLayoutParams(params);
                buttonSendVoice.setLayoutParams(params);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(message.getText().toString().isEmpty()){

                    params.weight = (float) 0.15;
                    buttonSendImage.setLayoutParams(params);
                    buttonSendVoice.setLayoutParams(params);
                }
            }
        });
    }

    private void setUpPresenter(){
        globalPresenter.checkDeviceId();
        globalPresenter.queryData(getContext());
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUpOnClickTouch(){
        buttonSend.setOnClickListener(this);
        buttonSendImage.setOnClickListener(this);
        buttonSendVoice.setOnTouchListener(this);
        rvGlobal.setOnTouchListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //unbinder.unbind();
        global_data.clear();
        if (childEventListener != null) {
            mGlobal.removeEventListener(childEventListener);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case OPEN_CAMERA:
                    globalPresenter.uploadImageFromCamera(uri);
                    break;
                case GALLERY_PICK:
                    String path = globalPresenter.getPath(getContext(), Uri.parse(data.getData().toString()));
                    globalPresenter.uploadImage(path);
                    break;
            }
        }
    }

    @Override
    public void getDataSuccess(ArrayList<MessageModel> data, ChildEventListener childEventListener) {

        this.childEventListener = childEventListener;
        this.global_data = data;

        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        globalAdapter.setGlobalAdapter(getContext(), global_data, mUsername, this);
        rvGlobal.setLayoutManager(linearLayoutManager);
        rvGlobal.setAdapter(globalAdapter);

        progressBar.setVisibility(View.GONE);
        globalAdapter.notifyDataSetChanged();

    }

    @Override
    public void setText() {
        message.setText("");
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void checkPermissionImageSuccess() {
        Intent pick_image = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pick_image, GALLERY_PICK);
    }

    @Override
    public void checkPermissionCameraSuccess() {
        String time = new SimpleDateFormat("dd-MM-yyyy_HH:MM:SS").format(new Date());
        String imageFileName = time + ".jpg";
        File imageFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String imagePath = imageFile.getAbsolutePath() + "/" + imageFileName;

        File file = new File(imagePath);
        uri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".provider", file);
        Intent open_camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        open_camera.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(open_camera, OPEN_CAMERA);
    }


    @Override
    public void initProgressDialog() {
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setMessage(String.valueOf(R.string.loading));
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, String.valueOf(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                globalPresenter.cancelUpload();
            }
        });
        mProgressDialog.show();
    }

    @Override
    public void setLoading(int i) {
        mProgressDialog.setProgress(i);
    }

    @Override
    public void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void overSize() {

        new AlertDialog.Builder(getContext())
                .setTitle(R.string.oversize5mb)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }


    @Override
    public void startRecording() {

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e("FAIL", "prepare() failed");
        }
        mRecorder.start();
    }

    @Override
    public void stopRecording() {
        try {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
            globalPresenter.uploadAudio(mFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void sendKeyMessage(final String key) {

        new AlertDialog.Builder(getContext())
                .setMessage(String.valueOf(R.string.want_delete))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        globalPresenter.delMessage(key);

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void playAudio(String url) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                }
            });
            mediaPlayer.prepare();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendUrl(final String key, final String url) {
        new AlertDialog.Builder(getContext())
                .setMessage(String.valueOf(R.string.want_delete))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        globalPresenter.deleteFileInStorage(key, url);

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void setZoomImage(String url) {

        Intent zoomImage = new Intent(getContext(), ImageActivity.class);
        zoomImage.putExtra("URL", url);
        startActivity(zoomImage);

    }


    public void hideKeyboard(View view) {
        message.clearFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_send:
                if (message.getText().toString().isEmpty()) {
                    message.setError(String.valueOf(R.string.please_enter_text));
                } else {
                    MessageModel data = new MessageModel();
                    data.setUsername(mUsername);
                    data.setMessage(message.getText().toString());
                    globalPresenter.sendMessage(data);
                }
                break;
            case R.id.button_send_image:
                final String open_camera = getResources().getString(R.string.open_camera);
                final String send_image = getResources().getString(R.string.send_image);
                final String send_video = getString(R.string.send_video);
                final CharSequence[] item = {open_camera, send_image, send_video};
                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                b.setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(item[i].equals(String.valueOf(R.string.open_camera))){
                            globalPresenter.checkPermissionCamera();
                        }else if(item[i].equals(String.valueOf(R.string.send_image))){
                            globalPresenter.checkPermissionImage();
                        }else if(item[i].equals(String.valueOf(R.string.send_video))){
                            Log.e("Video","Video");
                        }
                    }
                });
                b.show();
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_send_voice:
                globalPresenter.checkPermissionAudio(motionEvent);
                break;
            case R.id.rv_global:
                hideKeyboard(view);
                break;
        }
        return false;
    }
}
