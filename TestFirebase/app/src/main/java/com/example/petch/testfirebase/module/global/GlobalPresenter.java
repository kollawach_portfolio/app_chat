package com.example.petch.testfirebase.module.global;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.petch.testfirebase.model.MessageModel;
import com.example.petch.testfirebase.realm.RealmManage;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.HttpHeaders;
import cz.msebera.android.httpclient.entity.StringEntity;

public class GlobalPresenter implements GlobalConstructor.GlobalSetPresenter {

    private GlobalConstructor.View view;
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Global");
    private DatabaseReference mGlobal = databaseReference.child("Messages");
    private ChildEventListener childEventListener;
    private ArrayList<MessageModel> global_queryData = new ArrayList<>();
    private MessageModel global_data;

    private JSONArray registration_ids = new JSONArray();
    private Context mContext;
    private String deviceId = "";
    private String mUsername = "";
    //

    private UploadTask mUploadTask;

    GlobalPresenter(GlobalConstructor.View view) {
        this.view = view;
    }

    @Override
    public void queryData(Context context) {
        this.mContext = context;
        view.showLoading();
        childEventListener = mGlobal.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();

                try {
                    String key = dataSnapshot.getKey();
                    String username = newPost.get("user").toString();
                    String message = newPost.get("message").toString();
                    String time = newPost.get("time").toString();
                    String date = newPost.get("date").toString();
                    String type_message = "";
                    try{
                        type_message = newPost.get("type_message").toString();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    //global_data = new GlobalItem();
                    global_data = new MessageModel();
                    global_data.setKey(key);
                    global_data.setUsername(username);
                    global_data.setMessage(message);
                    global_data.setType_message(type_message);
                    global_data.setTime(time);
                    global_data.setDate(date);
                    global_queryData.add(global_data);

                    view.getDataSuccess(global_queryData, childEventListener);

                } catch (Exception e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                String delRoom = dataSnapshot.getKey();
                for(int i = 0; i < global_queryData.size(); i++){

                    if(delRoom.contentEquals(global_queryData.get(i).getKey())){

                        global_queryData.remove(i);
                    }
                }
                view.getDataSuccess(global_queryData,childEventListener);

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    @Override
    public void sendMessage(final MessageModel message) {

        //
        try {
            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
        }catch (Exception e){
            e.printStackTrace();
        }
        //

        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat postFormat = new SimpleDateFormat("HH:mm");
        String newDateStr = postFormat.format(currentTime);

        final Calendar c = Calendar.getInstance();
        int  mYear = c.get(Calendar.YEAR) + 543;
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String date = mDay+"/"+mMonth+"/"+ mYear;

        final Map<String, Object> chat = new HashMap<String, Object>();
        chat.put("user", message.getUsername());
        chat.put("message", message.getMessage());
        chat.put("time",newDateStr);
        chat.put("date",date);
        chat.put("deviceId",deviceId);
        mGlobal.push().setValue(chat);
        view.setText();


        //////////////เพิ่ม
        if (registration_ids.length() > 0) {

            String url = "https://fcm.googleapis.com/fcm/send";
            AsyncHttpClient client = new AsyncHttpClient();

            client.addHeader(HttpHeaders.AUTHORIZATION, "key=AIzaSyBE_svX7kJjOHXRhRKu1tSceiMDQyq7NRg");
            client.addHeader(HttpHeaders.CONTENT_TYPE, RequestParams.APPLICATION_JSON);

            try {

                JSONObject params = new JSONObject();
                params.put("registration_ids", registration_ids);

                JSONObject notificationObject = new JSONObject();
                notificationObject.put("body", message.getMessage());
                notificationObject.put("title", message.getUsername());

                params.put("notification", notificationObject);

                StringEntity entity = new StringEntity(params.toString(),"UTF-8");
                client.post(mContext, url, entity, RequestParams.APPLICATION_JSON, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {

                    }
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ///////////////////////

    }

    @Override
    public void checkDeviceId() {

        //
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersRef = reference.child("User");
        try {
            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
        }catch (Exception e){
            e.printStackTrace();
        }
        //

        final ValueEventListener userValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                registration_ids = new JSONArray();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    try {
                        Map<String, Object> newPost = (Map<String, Object>) postSnapshot.getValue();
                        String deviceIds = newPost.get("deviceId").toString();
                        String deviceToken = newPost.get("deviceToken").toString();

                        if (!deviceIds.contentEquals(deviceId) && !deviceToken.isEmpty()) {
                            registration_ids.put(deviceToken);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                registration_ids = new JSONArray();
            }
        };
        usersRef.addValueEventListener(userValueEventListener);

    }

    @Override
    public void checkPermissionImage() {
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            view.checkPermissionImageSuccess();

        } else{
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }
    }

    @Override
    public String getPath(Context context, Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    @Override
    public void uploadImage(String path) {
        Uri file = Uri.fromFile(new File(path));
        int size_file = Integer.parseInt(String.valueOf(new File(path).length() / 1024));
        if (size_file < 5120) {

            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            final StorageReference folderRef = storageRef.child("message").child("image").child(file.getLastPathSegment());
            mUploadTask = folderRef.putFile(file);

            view.initProgressDialog();

            mUploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                    view.setLoading(progress);
                }
            }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();

                    }
                    return folderRef.getDownloadUrl();
                }
            }).addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    view.dismissProgressDialog();
                    uploadImageSuccess(uri.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    view.dismissProgressDialog();
                    Toast.makeText(mContext, String.format("Failure: %s", e.getMessage()), Toast.LENGTH_SHORT).show();
                }
            });
        } else {

            view.overSize();
        }
    }

    @Override
    public void checkPermissionCamera() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
        {
            view.checkPermissionCameraSuccess();

        } else{
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{android.Manifest.permission.CAMERA}, 1);
        }
    }

    @Override
    public void uploadImageFromCamera(Uri path) {
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        final StorageReference folderRef = storageRef.child("message").child("image").child(path.getLastPathSegment());
        mUploadTask = folderRef.putFile(path);

        view.initProgressDialog();

        mUploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                view.setLoading(progress);
            }
        }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();

                }
                return folderRef.getDownloadUrl();
            }
        }).addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                view.dismissProgressDialog();
                uploadImageSuccess(uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                view.dismissProgressDialog();
                Toast.makeText(mContext, String.format("Failure: %s", e.getMessage()), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void uploadImageSuccess(String url) {
        try {

            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
            mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();
        }catch (Exception e){
            e.printStackTrace();
        }

        Date currentDateTime = Calendar.getInstance().getTime();
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        final String time = formatTime.format(currentDateTime);

        final Calendar c = Calendar.getInstance();
        int  mYear = c.get(Calendar.YEAR) + 543;
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        final String date = mDay+"/"+mMonth+"/"+ mYear;

        Map<String, Object> chat = new HashMap<String, Object>();
        chat.put("user", mUsername);
        chat.put("message",url);
        chat.put("type_message","image");
        chat.put("time",time);
        chat.put("date",date);
        chat.put("deviceId",deviceId);
        mGlobal.push().setValue(chat);
    }

    @Override
    public void cancelUpload() {
        mUploadTask.cancel();
    }

    @Override
    public void checkPermissionAudio(MotionEvent motionEvent) {
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
        {
            if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                view.startRecording();
            }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                view.stopRecording();
            }

        } else{
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO}, 111);
        }
    }

    @Override
    public void uploadAudio(String mFilename) {

        Uri uri = Uri.fromFile(new File(mFilename));
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference path_data = databaseReference.child("AUDIO").push();
        String path = path_data.getKey();

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        final StorageReference audio = storageRef.child("message").child("audio").child(path +".3gp");
        mUploadTask = audio.putFile(uri);

        view.initProgressDialog();

        mUploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                view.setLoading(progress);
            }
        }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return audio.getDownloadUrl();
            }
        }).addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                view.dismissProgressDialog();
                uploadAudioSuccess(uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                view.dismissProgressDialog();
                Toast.makeText(mContext, String.format("Failure: %s", e.getMessage()), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void uploadAudioSuccess(String url) {
        try {
            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
            mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();
        }catch (Exception e){
            e.printStackTrace();
        }

        Date currentDateTime = Calendar.getInstance().getTime();
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        final String time = formatTime.format(currentDateTime);

        final Calendar c = Calendar.getInstance();
        int  mYear = c.get(Calendar.YEAR) + 543;
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        final String date = mDay+"/"+mMonth+"/"+ mYear;

        Map<String, Object> chat = new HashMap<String, Object>();
        chat.put("user", mUsername);
        chat.put("message",url);
        chat.put("type_message","audio");
        chat.put("time",time);
        chat.put("date",date);
        chat.put("deviceId",deviceId);
        mGlobal.push().setValue(chat);
    }

    @Override
    public void delMessage(String key) {
        mGlobal.child(key).removeValue();
    }

    @Override
    public void deleteFileInStorage(String key, String url) {
        mGlobal.child(key).removeValue();
        FirebaseStorage mFirebaseStorage = FirebaseStorage.getInstance();
        StorageReference photoRef = mFirebaseStorage.getReferenceFromUrl(url);
        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(mContext, "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
