package com.example.petch.testfirebase.module.image;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.petch.testfirebase.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageActivity extends AppCompatActivity {

    @BindView(R.id.zoom_image)
    ImageView zoomImage;

    private String mZoomImage = "";
    private String mRoom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Bundle bundle = getIntent().getExtras();
        mZoomImage = bundle.getString("URL");
        mRoom = bundle.getString("ROOM");
        Glide.with(this).load(mZoomImage).into(zoomImage);
    }

    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }



}
