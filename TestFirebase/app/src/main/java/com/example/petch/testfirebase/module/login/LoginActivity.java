package com.example.petch.testfirebase.module.login;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.UserModel;
import com.example.petch.testfirebase.module.manage.ManageActivity;
import com.example.petch.testfirebase.module.name.NameActivity;
import com.example.petch.testfirebase.module.register.RegisterActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginConstructor.View, View.OnFocusChangeListener, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.username)
    EditText txtUsername;
    @BindView(R.id.password)
    EditText txtPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnRegister)
    TextView btnRegister;
    @BindView(R.id.btn_sing_in_google)
    SignInButton btnSingInGoogle;
    private ProgressDialog progressDialog;
    private LoginPresenter loginPresenter;
    private UserModel login_data;

    private GoogleApiClient googleApiClient;
    private FirebaseAuth firebaseAuth;
    public static final int SING_IN_CODE = 777;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginPresenter = new LoginPresenter(this);
        login_data = new UserModel();

        setUpGoogleLogin();
        setUpProgressDialog();
        setUpOnClickFocus();
        setUpPresenter();

    }

    private void setUpGoogleLogin(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        btnSingInGoogle.setSize(SignInButton.SIZE_WIDE);
        btnSingInGoogle.setColorScheme(SignInButton.COLOR_DARK);
    }

    private void setUpProgressDialog(){
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setTitle(R.string.log_in);
        progressDialog.setMessage(getString(R.string.please_wait));
    }

    private void setUpOnClickFocus(){
        btnRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnSingInGoogle.setOnClickListener(this);
        txtUsername.setOnFocusChangeListener(this);
        txtPassword.setOnFocusChangeListener(this);
    }

    private void setUpPresenter(){
        loginPresenter.checkDataForAuth(LoginActivity.this);
        loginPresenter.setAuth();
    }

    @Override
    protected void onDestroy() {
        progressClose();
        loginPresenter.removeListener();
        super.onDestroy();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.username:
                if (!hasFocus) {
                    hideKeyboard(view);
                }
                break;
            case R.id.password:
                if (!hasFocus) {
                    hideKeyboard(view);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnLogin:
                //loginData = new LoginData();
                login_data = new UserModel();
                login_data.setContext(LoginActivity.this);
                login_data.setUsername(txtUsername.getText().toString());
                login_data.setPassword(txtPassword.getText().toString());
                loginPresenter.checkUser(login_data);
                break;
            case R.id.btnRegister:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_sing_in_google:
                Intent sing_in_google = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(sing_in_google, SING_IN_CODE);
                break;
        }

    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        switch (view.getId()) {
            case R.id.checkbox_logged:
                if (checked) {
                    loginPresenter.checkKeepLogged(true);

                } else {
                    loginPresenter.checkKeepLogged(false);
                }
                break;
        }
    }

    @Override
    public void usernameIsNull() {
        txtUsername.setError(getString(R.string.enter_username));
    }

    @Override
    public void passwordIsNull() {
        txtPassword.setError(getString(R.string.enter_password));
    }

    @Override
    public void loginSuccess() {
        Intent intent = new Intent(LoginActivity.this, ManageActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void progressShow() {
        if (!isFinishing()) {
            progressDialog.show();
        }
    }

    @Override
    public void progressClose() {
        progressDialog.dismiss();
    }

    @Override
    public void user_passIsWrong() {
        new AlertDialog.Builder(LoginActivity.this)
                .setMessage(R.string.user_pass_wrong)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void internetNotConnecting() {
        new AlertDialog.Builder(LoginActivity.this)
                .setMessage(R.string.internet_not_connected)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .create()
                .show();
    }


    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //-----------------------------Auth--------------------------------------

    @Override
    public void loginAuthSuccessOne() {
        Intent intent = new Intent(LoginActivity.this, NameActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void loginAuthSuccessTwo() {
        Intent intent = new Intent(LoginActivity.this, ManageActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void loginAuthFail() {
        Toast.makeText(LoginActivity.this, R.string.fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginPresenter.activityResult(requestCode, data);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
