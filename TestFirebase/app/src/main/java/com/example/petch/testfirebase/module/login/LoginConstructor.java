package com.example.petch.testfirebase.module.login;


import android.content.Context;
import android.content.Intent;

import com.example.petch.testfirebase.model.UserModel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

public class LoginConstructor {
    public interface LoginSetPresenter{

        void checkUser(UserModel userModel);

        void checkKeepLogged(boolean check);

        void checkDataForAuth(Context context);

        void setAuth();

        void activityResult(int requestCode,Intent data);

        void handleSingInResult(GoogleSignInResult result);

        void setFirebaseWithGoogle(GoogleSignInAccount signInAccount);

        void removeListener();

        boolean checkInternet();
}

    public interface View{

        void usernameIsNull();

        void passwordIsNull();

        void loginSuccess();

        void progressShow();

        void progressClose();

        void user_passIsWrong();

        void internetNotConnecting();

        void loginAuthSuccessOne();

        void loginAuthSuccessTwo();

        void loginAuthFail();

    }
}
