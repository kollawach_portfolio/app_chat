package com.example.petch.testfirebase.module.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.petch.testfirebase.model.UserModel;
import com.example.petch.testfirebase.realm.RealmManage;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Map;

import static com.example.petch.testfirebase.module.login.LoginActivity.SING_IN_CODE;

public class LoginPresenter implements LoginConstructor.LoginSetPresenter {

    private LoginConstructor.View view;
    private Context mContext;
    private String mUsername;
    private String mPassword;
    private Boolean mCheck = false;
    private String mDeviceId = "";
    private DatabaseReference databaseReference;
    private DatabaseReference mLoginData;
    private RealmManage realmManage = RealmManage.getInstance();
    private UserModel login_checkUser;

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener firebaseListener;
    private ChildEventListener childEventListener;


    private ArrayList<UserModel> auth_checkUser = new ArrayList<>();
    private int countCheck = 0;
    private DatabaseReference mUser;
    private String keyAuth;
    private String emailAuth;
    private String usernameAuth;



    LoginPresenter(LoginConstructor.View view) {
        this.view = view;
    }


    @Override

    public void checkUser(UserModel userModel) {

        this.login_checkUser = userModel;
        realmManage.clearAll();

        mDeviceId =  Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        mContext = login_checkUser.getContext();
        mUsername = login_checkUser.getUsername();
        mPassword = login_checkUser.getPassword();

        if (mUsername.isEmpty()) {

            view.usernameIsNull();

        }else if(mPassword.isEmpty()) {

            view.passwordIsNull();

        }else{

            if (checkInternet()) {
                databaseReference = FirebaseDatabase.getInstance().getReference();
                mLoginData = databaseReference.child("User").child(mUsername);

                mLoginData.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Map<String, Object> data = (Map<String, Object>) dataSnapshot.getValue();
                        String key = dataSnapshot.getKey();

                            if (data != null) {
                                String mUsernameFirebase = data.get("Username").toString();
                                String mPasswordFirebase = data.get("Password").toString();

                                if (mUsername.contentEquals(mUsernameFirebase) && mPassword.contentEquals(mPasswordFirebase)) {

                                    if (mCheck) {
                                        realmManage.addRealmData(key,mUsernameFirebase, mPasswordFirebase, mDeviceId);
                                    } else {
                                        realmManage.addRealmData(key,mUsernameFirebase, "", mDeviceId);
                                    }

                                    mLoginData.child("deviceId").setValue(mDeviceId);
                                    mLoginData.child("deviceToken").setValue(FirebaseInstanceId.getInstance().getToken());

                                    view.progressShow();
                                    view.loginSuccess();

                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            view.progressClose();
                                        }
                                    }, 1300);

                                } else {

                                    view.user_passIsWrong();

                                } //end if username

                            } else {

                                view.user_passIsWrong();
                            }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            } else {

                view.internetNotConnecting();

            }//end if check internet


        }//end if check Text null null

    }

    @Override
    public boolean checkInternet() {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }

    @Override
    public void checkKeepLogged(boolean check) {
        this.mCheck = check;
    }


    //////////////////////////////////////////////////

    @Override
    public void checkDataForAuth(Context context) {
        this.mContext = context;
        realmManage = RealmManage.getInstance();
        realmManage.clearAll();
        mDeviceId =  Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        countCheck = 0;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mUser = databaseReference.child("User");
        childEventListener = mUser.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                keyAuth = "";
                emailAuth = "";
                usernameAuth = "";
                Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();
                keyAuth = dataSnapshot.getKey();
                try {
                    if(newPost != null) {
                        usernameAuth = newPost.get("Username").toString();
                        emailAuth = newPost.get("Email").toString();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                UserModel data = new UserModel();
                data.setKey(keyAuth);
                data.setEmail(emailAuth);
                data.setUsername(usernameAuth);
                auth_checkUser.add(data);
            }
            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                String key = dataSnapshot.getKey();
                for(int i = 0; i<auth_checkUser.size(); i++){

                    if(auth_checkUser.get(i).getKey().contentEquals(key)){
                        auth_checkUser.remove(i);
                    }
                }
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void setAuth() {
        firebaseListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    goToSuccess(user);
                }
            }
        };
        firebaseAuth.addAuthStateListener(firebaseListener);
    }

    @Override
    public void removeListener() {
        if(firebaseListener != null){
            firebaseAuth.removeAuthStateListener(firebaseListener);
        }

        if(childEventListener != null){
            mUser.removeEventListener(childEventListener);
        }
    }

    @Override
    public void activityResult(int requestCode, Intent data) {
        if (requestCode == SING_IN_CODE){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSingInResult(result);
        }
    }

    @Override
    public void handleSingInResult(GoogleSignInResult result) {
        if(result.isSuccess()){
            if(result.getSignInAccount() != null) {
                setFirebaseWithGoogle(result.getSignInAccount());
            }
        }else{
            view.loginAuthFail();
        }
    }

    @Override
    public void setFirebaseWithGoogle(GoogleSignInAccount signInAccount) {
        view.progressShow();
        AuthCredential credential = GoogleAuthProvider.getCredential(signInAccount.getIdToken(),null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            view.loginAuthFail();
                        }
                        view.progressClose();
                    }
                });

    }

    private void goToSuccess(FirebaseUser user) {
        usernameAuth = "";
        keyAuth = "";

        for (int i = 0; i < auth_checkUser.size(); i++) {

            if (auth_checkUser.get(i).getEmail().contentEquals(user.getEmail())) {
                countCheck++;
                usernameAuth = auth_checkUser.get(i).getUsername();
                keyAuth = auth_checkUser.get(i).getKey();
                break;
            }
        }
        if (countCheck == 0) {
            Log.e("countCheck","countCheck" + countCheck);

            view.loginAuthSuccessOne();
        } else if (countCheck > 0) {

            Log.e("countCheck","countCheck" + countCheck);

            mUser.child(keyAuth).child("deviceId").setValue(mDeviceId);
            mUser.child(keyAuth).child("deviceToken").setValue(FirebaseInstanceId.getInstance().getToken());
            realmManage.addRealmData(keyAuth,usernameAuth, "", mDeviceId);
            countCheck = 0;
            view.loginAuthSuccessTwo();
        }
    }

}


