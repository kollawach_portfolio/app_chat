package com.example.petch.testfirebase.module.main;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.petch.testfirebase.realm.RealmManage;
import com.example.petch.testfirebase.service.DestroyAppService;
import com.example.petch.testfirebase.module.manage.ManageActivity;
import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.module.login.LoginActivity;

import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class MainActivity extends AppCompatActivity implements MainConstructor.View{

    private MainPresenter mainPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpRealm();
        startService(new Intent(getBaseContext(), DestroyAppService.class));
        mainPresenter = new MainPresenter(this);

        loadLocale();
        try {
            Bundle bundle = getIntent().getExtras();
            String lang = bundle.getString("LANGUAGE");
            setLocale(lang);
            bundle.clear();
        }catch (Exception e){
            e.printStackTrace();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainPresenter.checkGoogleLogin();
                mainPresenter.checkLogged(MainActivity.this);
            }
        }, 2000);
    }

    private void setUpRealm(){
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        Realm.getInstance(config);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.delListener();
    }


    @Override
    public void keepLogin() {
        Intent intent = new Intent(MainActivity.this, ManageActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void notKeepLogin() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void googleLoginSuccess() {
        Intent intent = new Intent(MainActivity.this, ManageActivity.class);
        startActivity(intent);
        finish();
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor sh = getSharedPreferences("Setting", MODE_PRIVATE).edit();
        sh.putString("Lang", lang);
        sh.apply();
    }

    private void loadLocale() {
        SharedPreferences preferences = getSharedPreferences("Setting", MODE_PRIVATE);
        String language = preferences.getString("Lang", "");
        if(language.isEmpty()){
            language = "th";
            Log.e("LANG","NULL");
        }
        setLocale(language);
    }
}



