package com.example.petch.testfirebase.module.main;

import android.content.Context;

public class MainConstructor {

    public interface MainSetPresenter {

        void checkLogged(Context context);

        void checkGoogleLogin();

        void delListener();

        boolean checkInternet();
    }

    public interface View {

        void keepLogin();

        void notKeepLogin();

        void googleLoginSuccess();
    }
}
