package com.example.petch.testfirebase.module.main;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.example.petch.testfirebase.realm.RealmManage;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class MainPresenter implements MainConstructor.MainSetPresenter {

    private MainConstructor.View view;
    private Context mContext;
    private DatabaseReference databaseReference;
    private DatabaseReference mLoginData;
    private String mUsername = "";
    private String mPassword = "";
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseListener;

    public MainPresenter(MainConstructor.View view) {
        this.view = view;
    }

    @Override
    public void checkLogged(Context context) {
        this.mContext = context;

        if(!(RealmManage.getInstance().getRealmData().isEmpty())) {

            mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();
            mPassword = RealmManage.getInstance().getRealmData().get(0).getPassword();
        }

        if (checkInternet()) {

            databaseReference = FirebaseDatabase.getInstance().getReference();
            mLoginData = databaseReference.child("User").child(mUsername);

            if (!(mUsername.isEmpty() || mPassword.isEmpty())) {

                mLoginData.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Map<String, Object> data = (Map<String, Object>) dataSnapshot.getValue();

                        if (data != null) {
                            String Username = data.get("Username").toString();
                            String Password = data.get("Password").toString();

                            if (mUsername.contentEquals(Username) && mPassword.contentEquals(Password)) {

                                view.keepLogin();

                            }else {

                                view.notKeepLogin();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }else{

                view.notKeepLogin();
            }
        }
    }

    @Override
    public void checkGoogleLogin() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    view.googleLoginSuccess();
                }
            }
        };
        firebaseAuth.addAuthStateListener(firebaseListener);
    }

    @Override
    public void delListener() {
        if (firebaseListener != null){

            firebaseAuth.removeAuthStateListener(firebaseListener);
        }
    }


    @Override
    public boolean checkInternet() {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }
        return false;
    }
}
