package com.example.petch.testfirebase.module.manage;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.module.chat.ChatActivity;
import com.example.petch.testfirebase.module.friend.FriendFragment;
import com.example.petch.testfirebase.module.global.GlobalFragment;
import com.example.petch.testfirebase.module.login.LoginActivity;
import com.example.petch.testfirebase.module.main.MainActivity;
import com.example.petch.testfirebase.module.room.RoomFragment;
import com.example.petch.testfirebase.realm.RealmManage;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManageActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.chat_layout)
    DrawerLayout drawer;

    private boolean doubleBackToExitPressedOnce = false;
    private String key = "";
    private String username = "";
    private GoogleApiClient googleApiClient;
    private FirebaseAuth firebaseAuth;

    private RealmManage realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setUpGoogleLogin();
        setUpSectionsPagerAdapter();
        setUpNavigationDrawer();

        firebaseAuth = FirebaseAuth.getInstance();
        realm = RealmManage.getInstance();
        try {
            key = RealmManage.getInstance().getRealmData().get(0).getKey();
            username = firebaseAuth.getCurrentUser().getDisplayName();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        tv_global.setTypeface(tv_global.getTypeface(), Typeface.BOLD);
//        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
//        t.replace(R.id.fl_manage, GlobalFragment.newInstance());
//        t.commit();
//
//
//        tv_global.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                tv_global.setTypeface(tv_global.getTypeface(), Typeface.BOLD);
//                tv_friend.setTypeface(tv_friend.getTypeface(), Typeface.NORMAL);
//                tv_room.setTypeface(tv_room.getTypeface(), Typeface.NORMAL);
//                FragmentTransaction t = getSupportFragmentManager().beginTransaction();
//                t.replace(R.id.fl_manage, GlobalFragment.newInstance());
//                t.commit();
//
//            }
//        });
//
//        tv_friend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                tv_global.setTypeface(tv_global.getTypeface(), Typeface.NORMAL);
//                tv_friend.setTypeface(tv_friend.getTypeface(), Typeface.BOLD);
//                tv_room.setTypeface(tv_room.getTypeface(), Typeface.NORMAL);
//                FragmentTransaction t = getSupportFragmentManager().beginTransaction();
//                t.replace(R.id.fl_manage, FriendFragment.newInstance());
//                t.commit();
//
//            }
//        });
//
//
//        tv_room.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                tv_global.setTypeface(tv_global.getTypeface(), Typeface.NORMAL);
//                tv_friend.setTypeface(tv_friend.getTypeface(), Typeface.NORMAL);
//                tv_room.setTypeface(tv_room.getTypeface(), Typeface.BOLD);
//                FragmentTransaction t = getSupportFragmentManager().beginTransaction();
//                t.replace(R.id.fl_manage, RoomFragment.newInstance());
//                t.commit();
//
//            }
//        });
    }

    private void setUpNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        View headerView = navigationView.getHeaderView(0);
        TextView txt = (TextView) headerView.findViewById(R.id.txt);
        txt.setText(RealmManage.getInstance().getRealmData().get(0).getUsername());
        navigationView.setNavigationItemSelectedListener(this);
    }


    private void setUpGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


    private void setUpSectionsPagerAdapter() {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(GlobalFragment.newInstance());
        adapter.addFragment(FriendFragment.newInstance());
        adapter.addFragment(RoomFragment.newInstance());

        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ((realm.getRealmData().isEmpty() || realm.getRealmData().get(0).getPassword().isEmpty()) && username.isEmpty()) {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
            databaseReference.child(key).child("deviceId").setValue(null);
            databaseReference.child(key).child("deviceToken").setValue(null);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.logout:
                new AlertDialog.Builder(ManageActivity.this)
                        .setMessage(getString(R.string.want_to_logout))
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                realm.clearAll();
                                firebaseAuth.signOut();
                                username = "";
                                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(@NonNull Status status) {
                                        if (status.isSuccess()) {
                                            Intent intent = new Intent(ManageActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Toast.makeText(ManageActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create()
                        .show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.press_again_to_exit, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.setting_change_language:
                final String thai = getString(R.string.thai);
                final String eng = getString(R.string.eng);
                final CharSequence[] item_language = {thai, eng};
                AlertDialog.Builder builder_language = new AlertDialog.Builder(ManageActivity.this);
                builder_language.setItems(item_language, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (item_language[i].equals(thai)) {
                            Intent lang = new Intent(ManageActivity.this, MainActivity.class);
                            lang.putExtra("LANGUAGE","th");
                            startActivity(lang);
                            finish();
                        } else if (item_language[i].equals(eng)) {
                            Intent lang = new Intent(ManageActivity.this, MainActivity.class);
                            lang.putExtra("LANGUAGE","en");
                            startActivity(lang);
                            finish();
                        }
                    }
                });
                builder_language.show();
                break;

            case R.id.setting_logout:
                new AlertDialog.Builder(ManageActivity.this)
                        .setMessage(getString(R.string.want_to_logout))
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                realm.clearAll();
                                firebaseAuth.signOut();
                                username = "";
                                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(@NonNull Status status) {
                                        if (status.isSuccess()) {
                                            Intent intent = new Intent(ManageActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Toast.makeText(ManageActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });


                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create()
                        .show();
                break;
        }
        return false;
    }
}
