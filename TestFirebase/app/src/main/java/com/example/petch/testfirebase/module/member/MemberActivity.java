package com.example.petch.testfirebase.module.member;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.UserModel;
import com.example.petch.testfirebase.module.manage.ManageActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberActivity extends AppCompatActivity implements MemberConstructor.View, MemberConstructor.DelMember {

    @BindView(R.id.rv_member)
    RecyclerView mRecyclerView;
    @BindView(R.id.member_invite)
    FloatingActionButton member_invite;

    private MemberPresenter memberPresenter;
    private MemberAdapter memberAdapter;
    private static String mRoom = "";
    private String mId = "";
    private EditText mInvite;
    private ArrayList<UserModel> member_data = new ArrayList<>();
    private ChildEventListener childEventListener;
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mDataRoom = databaseReference.child("Room");
    private DatabaseReference mUserRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);
        ButterKnife.bind(this);

        memberPresenter = new MemberPresenter(this);
        memberAdapter = new MemberAdapter();

        Bundle bundle = getIntent().getExtras();
        mRoom = bundle.getString("ROOM");
        mUserRoom = mDataRoom.child(mRoom).child("User");
        memberPresenter.queryData(mRoom);

        setUpOnClick();

    }

    private void setUpOnClick(){

        member_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MemberActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.template_invite_people, null);

                mInvite = (EditText) view.findViewById(R.id.invite_room);

                builder.setView(view)
                        .setTitle(R.string.invite)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                mId = mInvite.getText().toString();
                                memberPresenter.invitePeople(mId);

                            }
                        })
                        .create()
                        .show();


            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        if (childEventListener != null) {
            mUserRoom.removeEventListener(childEventListener);
        }
    }

    @Override
    public void getDataSuccess(ArrayList<UserModel> data, ChildEventListener childEventListener) {
        this.childEventListener = childEventListener;
        this.member_data = data;

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        memberAdapter.setMemberAdapter(MemberActivity.this, member_data, this);
        //linearLayoutManager.setReverseLayout(true);       //ทำให้ข้อความแสดงจากบนลงล่าง
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(memberAdapter);
        memberAdapter.notifyDataSetChanged();

    }

    @Override
    public void invitePeopleSuccess() {
        new AlertDialog.Builder(MemberActivity.this)
                .setMessage(R.string.invite_success)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void invitePeopleFail() {
        new AlertDialog.Builder(MemberActivity.this)
                .setMessage(String.valueOf(R.string.user_in_room))
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void delSuccess() {
        new AlertDialog.Builder(MemberActivity.this)
                .setMessage(String.valueOf(R.string.leave_success))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(MemberActivity.this, ManageActivity.class);
                        startActivity(intent);
                    }
                })
                .create()
                .show();
    }


    @Override
    public void delRoom(final String user) {
        new AlertDialog.Builder(MemberActivity.this)
                .setMessage(String.valueOf(R.string.want_delete_room))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        memberPresenter.delMember(user);

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();

    }
}
