package com.example.petch.testfirebase.module.member;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.UserModel;

import java.util.ArrayList;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.viewHolder> {

    private Context context;
    private ArrayList<UserModel> member;
    private MemberConstructor.DelMember delMember;

   public void setMemberAdapter (Context context, ArrayList<UserModel> data,MemberConstructor.DelMember delMember){
        this.context = context;
        this.member = data;
        this.delMember = delMember;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_member, viewGroup,false);
        return new MemberAdapter.viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int position) {

        final String username = member.get(position).getUsername();
        viewHolder.member_username.setText(username);

        viewHolder.member_delMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                delMember.delRoom(username);

            }
        });

    }

    @Override
    public int getItemCount() {
        if(member == null)
            return 0;
        if(member.isEmpty())
            return 0;
        return member.size();
    }


    static class viewHolder extends RecyclerView.ViewHolder{
        TextView member_username;
        TextView member_delMember;

        public viewHolder(View itemView) {
            super(itemView);
            member_username = itemView.findViewById(R.id.member_username);
            member_delMember = itemView.findViewById(R.id.member_delMember);

        }
    }
}
