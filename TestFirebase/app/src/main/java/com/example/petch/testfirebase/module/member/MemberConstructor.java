package com.example.petch.testfirebase.module.member;

import com.example.petch.testfirebase.model.UserModel;
import com.google.firebase.database.ChildEventListener;

import java.util.ArrayList;

public class MemberConstructor {

    public interface  MemberSetPresenter{

        void queryData(String mRoom);

        void delMember(String user);

        void invitePeople(String id);

    }

    public interface View {

        void getDataSuccess(ArrayList<UserModel> data, ChildEventListener childEventListener);

        void invitePeopleSuccess();

        void invitePeopleFail();

        void delSuccess();

    }

    public interface DelMember {

        void delRoom(String user);

    }
}
