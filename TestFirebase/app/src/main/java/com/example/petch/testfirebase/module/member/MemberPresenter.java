package com.example.petch.testfirebase.module.member;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.petch.testfirebase.model.UserModel;
import com.example.petch.testfirebase.realm.RealmManage;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MemberPresenter implements MemberConstructor.MemberSetPresenter{

    private MemberConstructor.View view;
    private ChildEventListener childEventListener;
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mDataRoom =  databaseReference.child("Room");
    private DatabaseReference mNameRoom;
    private DatabaseReference mUserRoom;
    private ArrayList<UserModel> member_queryData = new ArrayList<>();
    private String username;
    private int countInvite = 0;

    MemberPresenter(MemberConstructor.View view){ this.view = view; }

    @Override
    public void queryData(String mRoom) {
        mUserRoom = mDataRoom.child(mRoom).child("User");
        mNameRoom = mDataRoom.child(mRoom);

        childEventListener = mUserRoom.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                username = "";
                username = dataSnapshot.getKey();

                UserModel data = new UserModel();
                data.setUsername(username);
                member_queryData.add(data);
                view.getDataSuccess(member_queryData,childEventListener);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                Log.e("REMOVE","Member");

                String delRoom = dataSnapshot.getKey();
                for(int i = 0; i < member_queryData.size(); i++){
                    if(delRoom.contentEquals(member_queryData.get(i).getUsername())){
                        member_queryData.remove(i);
                    }
                }
                view.getDataSuccess(member_queryData,childEventListener);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void delMember(String user) {
        try {
            String username = RealmManage.getInstance().getRealmData().get(0).getUsername();
            if(user.contentEquals(username)){
                if(member_queryData.size() == 1){
                    mNameRoom.removeValue();
                    view.delSuccess();
                }else if(member_queryData.size() > 1) {
                    mUserRoom.child(user).removeValue();
                    view.delSuccess();
                }

            }else {
                mUserRoom.child(user).removeValue();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void invitePeople(String id) {
        countInvite = 0;
        for (int i = 0; i < member_queryData.size(); i++) {
            if (id.contentEquals(member_queryData.get(i).getUsername())) {
                countInvite++;
                break;
            }
        }

        if (countInvite > 0) {
            view.invitePeopleFail();
        } else {
            if (!id.isEmpty()) {
                mUserRoom.child(id).setValue("");
                view.invitePeopleSuccess();
            }
        }

    }
}
