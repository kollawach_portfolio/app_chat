package com.example.petch.testfirebase.module.name;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.module.manage.ManageActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NameActivity extends AppCompatActivity implements NameConstructor.View, GoogleApiClient.OnConnectionFailedListener {


    @BindView(R.id.name_username)
    EditText nameUsername;
    @BindView(R.id.btn_ok)
    Button btnOk;
    @BindView(R.id.cl_activity_name)
    ConstraintLayout clActivityName;
    private GoogleApiClient googleApiClient;
    private FirebaseAuth firebaseAuth;
    private NamePresenter namePresenter;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean checkLogout = true;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        ButterKnife.bind(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        firebaseAuth = FirebaseAuth.getInstance();

        namePresenter = new NamePresenter(this);
        namePresenter.checkUserAuth();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = nameUsername.getText().toString();
                namePresenter.checkName(username, NameActivity.this);

            }
        });
        clActivityName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(view);
                return false;
            }
        });
    }

    @Override
    protected void onStop() {
        if(checkLogout) {
            logoutGoogle();
        }
        namePresenter.removeListener();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_again_to_exit, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void setUsernameToDataIsSuccess() {
        checkLogout = false;
        Intent intent = new Intent(NameActivity.this, ManageActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void usernameIsAlready() {
        Toast.makeText(this, R.string.user_already_exists, Toast.LENGTH_SHORT).show();
    }

    private void logoutGoogle() {
        firebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (!status.isSuccess()) {
                    Toast.makeText(NameActivity.this, R.string.fail, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
