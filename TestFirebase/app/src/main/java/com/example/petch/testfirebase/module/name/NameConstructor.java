package com.example.petch.testfirebase.module.name;

import android.content.Context;

public class NameConstructor {

    public interface  NameSetPresenter{

        void checkUserAuth();

        void checkName(String username,Context context);

        void setUsernameToData();

        void removeListener();

    }

    public interface View {

        void setUsernameToDataIsSuccess();

        void usernameIsAlready();


    }

}
