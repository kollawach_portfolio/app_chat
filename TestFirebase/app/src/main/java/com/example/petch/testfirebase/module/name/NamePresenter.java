package com.example.petch.testfirebase.module.name;

import android.content.Context;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.petch.testfirebase.model.UserModel;
import com.example.petch.testfirebase.realm.RealmManage;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NamePresenter implements NameConstructor.NameSetPresenter{

    private NameConstructor.View view;
    private Context mContext;
    private ArrayList<UserModel> name_checkUser = new ArrayList<>();
    private String keyAuth;
    private String emailAuth;
    private String usernameAuth;
    private String mDeviceId = "";
    private String mEmail = "";
    private String mUsername = "";
    private DatabaseReference databaseReference;
    private DatabaseReference mUser;
    private ChildEventListener childEventListener;
    private RealmManage realmManage;
    private FirebaseAuth firebaseAuth;
    private int countCheck = 0;

    NamePresenter(NameConstructor.View view) {
        this.view = view;
    }

    @Override
    public void checkUserAuth() {
        realmManage = RealmManage.getInstance();
        realmManage.clearAll();

        databaseReference = FirebaseDatabase.getInstance().getReference();
        mUser = databaseReference.child("User");
        childEventListener = mUser.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                keyAuth = "";
                emailAuth = "";
                usernameAuth = "";
                Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();
                keyAuth = dataSnapshot.getKey();
                try {
                    if(newPost != null) {
                        usernameAuth = newPost.get("Username").toString();
                        emailAuth = newPost.get("Email").toString();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                UserModel data = new UserModel();
                data.setKey(keyAuth);
                data.setEmail(emailAuth);
                data.setUsername(usernameAuth);
                name_checkUser.add(data);

                if(emailAuth.contentEquals(mEmail) && usernameAuth.contentEquals(mUsername)){
                    realmManage.addRealmData(keyAuth, usernameAuth, "", mDeviceId);
                    view.setUsernameToDataIsSuccess();
                }

            }
            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void checkName(String username, Context context) {
        this.mContext = context;
        this.mUsername = username;

        if(name_checkUser.isEmpty()) {
            setUsernameToData();

        }else{

            for (int i =0; i<name_checkUser.size(); i++){

                if(name_checkUser.get(i).getUsername().contentEquals(mUsername)){
                    countCheck++;
                }

            }

            if(countCheck == 0){
                setUsernameToData();
            }else if(countCheck > 0){
                view.usernameIsAlready();
                countCheck = 0;
            }

        }

    }


    @Override
    public void setUsernameToData() {

        firebaseAuth = FirebaseAuth.getInstance();
        mDeviceId =  Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        mEmail = firebaseAuth.getCurrentUser().getEmail();

        Map<String, Object> user = new HashMap<String, Object>();
        user.put("Username", mUsername);
        user.put("Password", "");
        user.put("Email", mEmail);
        user.put("deviceId", Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID));
        user.put("deviceToken", FirebaseInstanceId.getInstance().getToken());

        mUser.push().setValue(user);

    }

    @Override
    public void removeListener() {
        if(childEventListener != null){
            mUser.removeEventListener(childEventListener);
        }
    }
}
