package com.example.petch.testfirebase.module.register;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.UserModel;
import com.example.petch.testfirebase.module.login.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity implements RegisterConstructor.View, View.OnFocusChangeListener, View.OnClickListener {

    @BindView(R.id.username)
    EditText txtUsername;
    @BindView(R.id.password)
    EditText txtPassword;
    @BindView(R.id.repeat_password)
    EditText txtRepeat_Password;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    private RegisterPresenter registerPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        setTitle("Register");

        registerPresenter = new RegisterPresenter(this);
        registerPresenter.getUsername();

        setUpOnClickFocus();
    }

    private void setUpOnClickFocus(){
        btnRegister.setOnClickListener(this);
        txtUsername.setOnFocusChangeListener(this);
        txtPassword.setOnFocusChangeListener(this);
        txtRepeat_Password.setOnFocusChangeListener(this);
    }

    @Override
    public void textIsNull() {
        new AlertDialog.Builder(RegisterActivity.this)
                .setTitle(R.string.please_enter)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void passwordIsWrong() {
        new AlertDialog.Builder(RegisterActivity.this)
                .setTitle(R.string.current_password_wrong)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void usernameIsAlready() {
        new AlertDialog.Builder(RegisterActivity.this)
                .setTitle(R.string.user_already_exists)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void getLogin() {

        txtUsername.setText("");
        txtPassword.setText("");
        txtRepeat_Password.setText("");

        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {

        switch (view.getId()) {
            case R.id.username:
                if (!hasFocus) {
                    hideKeyboard(view);
                }
                break;
            case R.id.password:
                if (!hasFocus) {
                    hideKeyboard(view);
                }
                break;
            case R.id.repeat_password:
                if (!hasFocus) {
                    hideKeyboard(view);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnRegister:
                UserModel register_data = new UserModel();
                register_data.setUsername(txtUsername.getText().toString());
                register_data.setPassword(txtPassword.getText().toString());
                register_data.setRepeat_Password(txtRepeat_Password.getText().toString());
                registerPresenter.checkRegister(register_data);
                break;
        }

    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
