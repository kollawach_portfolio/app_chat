package com.example.petch.testfirebase.module.register;


import com.example.petch.testfirebase.model.UserModel;

public class RegisterConstructor {

    public interface RegisterSetPresenter{

        void getUsername();

        void checkRegister(UserModel userModel);

        void registerSuccess();
    }

    public interface View {

        void textIsNull();

        void passwordIsWrong();

        void usernameIsAlready();

        void getLogin();
    }

}
