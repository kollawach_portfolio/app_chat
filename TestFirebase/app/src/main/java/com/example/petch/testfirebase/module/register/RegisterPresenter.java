package com.example.petch.testfirebase.module.register;


import com.example.petch.testfirebase.model.UserModel;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


public class RegisterPresenter implements RegisterConstructor.RegisterSetPresenter {

    private RegisterConstructor.View view;
    private DatabaseReference databaseReference =  FirebaseDatabase.getInstance().getReference();;
    private DatabaseReference mRegister = databaseReference.child("User");
    private String mUsername;
    private String mPassword;
    private String mRepeat_Password;
    private int countCheck = 0;
    private ArrayList<UserModel> register_checkUser = new ArrayList<>();
    private ChildEventListener childEventListener;


    public RegisterPresenter(RegisterConstructor.View view){ this.view = view; }

    @Override
    public void getUsername() {

        childEventListener = mRegister.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String username = dataSnapshot.getKey();
                UserModel data = new UserModel();
                data.setUsername(username);
                register_checkUser.add(data);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }


            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                String key = dataSnapshot.getKey();

                for(int i = 0; i < register_checkUser.size(); i++){

                    if (key.contentEquals(register_checkUser.get(i).getUsername())) {
                        register_checkUser.remove(i);
                    }

                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void checkRegister(UserModel userModel) {

        if(childEventListener != null){
            mRegister.removeEventListener(childEventListener);
        }

        mUsername = userModel.getUsername();
        mPassword = userModel.getPassword();
        mRepeat_Password = userModel.getRepeat_Password();

        if (mUsername.isEmpty() || mPassword.isEmpty() || mRepeat_Password.isEmpty()) {

            view.textIsNull();

        } else {

            if (mPassword.contentEquals(mRepeat_Password)) {

                if(register_checkUser.isEmpty()) {
                    registerSuccess();

                }else{
                    for (int i =0; i<register_checkUser.size(); i++){

                        if(register_checkUser.get(i).getUsername().contentEquals(mUsername)){
                            countCheck++;
                        }
                    }
                    if(countCheck == 0){
                        registerSuccess();
                    }else if(countCheck > 0){
                        view.usernameIsAlready();
                        countCheck = 0;
                    }

                }
            } else {

                view.passwordIsWrong();

            }
        }
    }

    @Override
    public void registerSuccess() {
        mRegister.child(mUsername).child("Username").setValue(mUsername);
        mRegister.child(mUsername).child("Password").setValue(mPassword);

        view.getLogin();
    }


}
