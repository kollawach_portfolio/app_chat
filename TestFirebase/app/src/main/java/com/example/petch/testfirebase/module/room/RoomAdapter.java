package com.example.petch.testfirebase.module.room;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.RoomModel;

import java.util.ArrayList;


public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.viewHolder>{

    private ArrayList<RoomModel> room_setData;
    private RoomConstructor.DelRoom delRoom;

    public void setChatAdapter( ArrayList<RoomModel> data,RoomConstructor.DelRoom delRoom){
        this.room_setData = data;
        this.delRoom = delRoom;
    }

    @NonNull
    @Override
    public RoomAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_room, viewGroup,false);
        return new RoomAdapter.viewHolder(v);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull RoomAdapter.viewHolder viewHolder, final int position) {

        final String room = room_setData.get(position).getRoom();
        final String password = room_setData.get(position).getPassword();

        final String emoticon = room_setData.get(position).getEmoticon();

        viewHolder.txtRoom.setText(room);
        viewHolder.iv_openRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(password.isEmpty()) {
                    delRoom.passwordIsEmpty(room,emoticon);
                }else{
                    delRoom.getPassword(room,password,emoticon);
                }
            }
        });

        viewHolder.iv_delRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delRoom.delRoom(room);
            }
        });

        viewHolder.txtRoom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {
                delRoom.hideKeyboard();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        if(room_setData == null)
            return 0;
        if (room_setData.isEmpty())
            return 0;
        return room_setData.size();
    }


    static class viewHolder extends RecyclerView.ViewHolder{
        TextView txtRoom;
        ImageView iv_openRoom;
        TextView iv_delRoom;

         viewHolder(View itemView) {
            super(itemView);
            txtRoom = itemView.findViewById(R.id.txt_room);
            iv_openRoom = itemView.findViewById(R.id.openRoom);
            iv_delRoom = itemView.findViewById(R.id.delRoom);

            txtRoom.setText("");
        }
    }


}
