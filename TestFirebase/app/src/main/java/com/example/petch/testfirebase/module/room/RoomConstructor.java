package com.example.petch.testfirebase.module.room;

import com.example.petch.testfirebase.model.RoomModel;
import com.google.firebase.database.ChildEventListener;

import java.util.ArrayList;

public class RoomConstructor {

    public interface  RoomSetPresenter{

        void queryData(String search);

        void addRoom(String room,String password);

        void checkPassword(String passwordOne, String passwordTwo);

        void delRoom(String room);

    }

    public interface View {

        void getDataSuccess(ArrayList<RoomModel> roomModels, ChildEventListener childEventListener);

        void setAdapter();

        void setArrayList();

        void checkPasswordSuccess();

        void checkPasswordSFail();

        void addRoomFail();

    }

    public interface DelRoom {

        void delRoom(String room);

        void passwordIsEmpty(String room,String emoticon);

        void getPassword(String room,String password,String emoticon);

        void hideKeyboard();
    }
}
