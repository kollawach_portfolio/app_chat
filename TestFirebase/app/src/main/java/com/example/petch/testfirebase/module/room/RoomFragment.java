package com.example.petch.testfirebase.module.room;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.model.RoomModel;
import com.example.petch.testfirebase.module.chat.ChatActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RoomFragment extends Fragment implements RoomConstructor.View, RoomConstructor.DelRoom {

    @BindView(R.id.rv_room)
    RecyclerView rv_room;
    Unbinder unbinder;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.txt_search)
    EditText txtSearch;
    @BindView(R.id.button_search)
    Button buttonSearch;

    private String mSearch = "";

    private RoomAdapter roomAdapter;
    private ArrayList<RoomModel> room_data = new ArrayList<>();
    private RoomPresenter roomPresenter;
    private ChildEventListener childRoom;
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mDataRoom = databaseReference.child("Room");

    private EditText room_name;
    private EditText room_password;
    private String roomName = null;
    private String password = null;

    private EditText check_password;
    private String mRoom = "";
    private String mPassword = "";

    private String mEmoticon = "";

    private View view_hide;

    public static RoomFragment newInstance() {
        return new RoomFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        roomAdapter = new RoomAdapter();
        roomPresenter = new RoomPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view_hide = inflater.inflate(R.layout.fragment_room, container, false);
        unbinder = ButterKnife.bind(this, view_hide);
        return view_hide;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        roomPresenter.queryData(mSearch);
        setUpTextChanged();
        setUpOnClickTouch();
    }

    private void setUpTextChanged(){
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                mSearch = txtSearch.getText().toString();
                roomPresenter.queryData(mSearch);
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUpOnClickTouch(){

//        buttonSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mSearch = txtSearch.getText().toString();
//                roomPresenter.queryData(mSearch);
//            }
//        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAddRoom();
            }
        });

        rv_room.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mSearch = "";
        roomPresenter.queryData(mSearch);
    }

    @Override
    public void onDestroyView() {
        if (childRoom != null) {
            mDataRoom.removeEventListener(childRoom);
        }
        super.onDestroyView();
        //unbinder.unbind();
        room_data.clear();
        hideKeyboard();
    }

    private void setAddRoom() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.template_add_room, null);
        room_name = view.findViewById(R.id.room_name);
        room_password = view.findViewById(R.id.room_password);
        builder.setView(view)
                .setTitle(R.string.add_room)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        roomName = room_name.getText().toString();
                        password = room_password.getText().toString();
                        roomPresenter.addRoom(roomName, password);

                    }
                })
                .create()
                .show();
    }

    @Override
    public void getDataSuccess(ArrayList<RoomModel> data, ChildEventListener childEventListener) {

        this.childRoom = childEventListener;
        this.room_data = data;

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        roomAdapter.setChatAdapter(room_data, this);
        rv_room.setLayoutManager(linearLayoutManager);
        rv_room.setAdapter(roomAdapter);
        roomAdapter.notifyDataSetChanged();
    }

    @Override
    public void setAdapter() {
        roomAdapter = new RoomAdapter();
    }

    @Override
    public void setArrayList() {
        room_data = new ArrayList<>();
    }

    @Override
    public void checkPasswordSuccess() {

        Intent intent = new Intent(getContext(), ChatActivity.class);
        intent.putExtra("REF", "Room");
        intent.putExtra("ROOM", mRoom);
        intent.putExtra("EMOTICON", mEmoticon);
        startActivity(intent);
    }

    @Override
    public void checkPasswordSFail() {

        new AlertDialog.Builder(getContext())
                .setMessage(String.valueOf(R.string.pass_wrong))
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void addRoomFail() {
        Snackbar.make(getView(), R.string.room_already_exists, Snackbar.LENGTH_LONG).show();
    }


    @Override
    public void delRoom(final String room) {

        new AlertDialog.Builder(getContext())
                .setMessage(String.valueOf(R.string.want_delete_room))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        roomPresenter.delRoom(room);

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void passwordIsEmpty(String room, String emoticon) {

        Intent intent = new Intent(getContext(), ChatActivity.class);
        intent.putExtra("REF", "Room");
        intent.putExtra("ROOM", room);
        intent.putExtra("EMOTICON", emoticon);
        startActivity(intent);
    }

    @Override
    public void getPassword(String room, final String password, String emoticon) {
        this.mRoom = room;
        this.mEmoticon = emoticon;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.templare_check_password, null);

        check_password = view.findViewById(R.id.check_password);

        builder.setView(view)
                .setTitle(R.string.password)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        mPassword = check_password.getText().toString();
                        roomPresenter.checkPassword(password, mPassword);
                    }
                })
                .create()
                .show();
    }

    @Override
    public void hideKeyboard() {

        Log.e("hideKeyboard","hideKeyboard");
        txtSearch.clearFocus();
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view_hide.getWindowToken(), 0);
    }
}
