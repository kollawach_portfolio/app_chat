package com.example.petch.testfirebase.module.room;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.petch.testfirebase.model.RoomModel;
import com.example.petch.testfirebase.realm.RealmManage;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Map;

public class RoomPresenter implements RoomConstructor.RoomSetPresenter{

    private RoomConstructor.View view;
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mDataRoom =  databaseReference.child("Room");
    private ArrayList<RoomModel> room_addRoom = new ArrayList<>();
    private ArrayList<RoomModel> room_queryData = new ArrayList<>();
    private ChildEventListener childEventListenerDataRoom;

    private String key;
    private String password;
    private String username;

    private String mUsername = "";
    private RoomModel room_roomData;

    private int countCheckRoom = 0;


    private String emoticon;

    RoomPresenter(RoomConstructor.View view){ this.view = view; }

    @Override
    public void queryData(String search) {
        room_queryData = new ArrayList<>();

        Log.e("SEARCH","SEARCH " + search + " SIZE" + room_queryData.size());

        Query query = mDataRoom.orderByKey().startAt(search).endAt(search + "\uf8ff");
        childEventListenerDataRoom = query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                key = "";
                password = "";
                emoticon = "";
                key = dataSnapshot.getKey();
                try {
                Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();
                    if(newPost != null) {
                            password = newPost.get("Password").toString();
                            emoticon = newPost.get("Emoticon").toString();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Log.e("KEY0",key);
                //RoomItem room_data = new RoomItem();
                RoomModel room_data = new RoomModel();
                room_data.setRoom(key);
                room_addRoom.add(room_data);
                checkUserForSetAdapter(key,password,emoticon);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                String delRoom = dataSnapshot.getKey();
                Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();
                if(newPost != null) {
                    try {
                        password = newPost.get("Password").toString();
                        emoticon = newPost.get("Emoticon").toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                for(int i = 0; i < room_queryData.size(); i++){
                    if(delRoom.contentEquals(room_queryData.get(i).getRoom())){
                        room_queryData.get(i).setPassword(password);
                        room_queryData.get(i).setEmoticon(emoticon);
                    }
                }
                view.getDataSuccess(room_queryData,childEventListenerDataRoom);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.e("REMOVE","Room1");
                String delRoom = dataSnapshot.getKey();
                for(int i = 0; i < room_addRoom.size(); i++){
                    if(delRoom.contentEquals(room_addRoom.get(i).getRoom())){
                        room_addRoom.remove(i);
                    }
                }
                for(int i = 0; i < room_queryData.size(); i++){
                    if(delRoom.contentEquals(room_queryData.get(i).getRoom())){
                        room_queryData.remove(i);
                    }
                }

                view.getDataSuccess(room_queryData,childEventListenerDataRoom);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    private void checkUserForSetAdapter(final String room, final String password,final String emoticon){
        DatabaseReference mUserRoom = mDataRoom.child(room).child("User");
        try {
            mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();
        }catch (Exception e){
            e.printStackTrace();
        }

        //Log.e("KEY","TEST");
        mUserRoom.addChildEventListener(new ChildEventListener() {
          @Override
          public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
              //Log.e("KEY1",key);
              username = "";
              username = dataSnapshot.getKey();

              //room_roomData = new RoomItem();
              room_roomData = new RoomModel();
              if (username.contentEquals(mUsername)) {

                  if(username.startsWith(mUsername))

                  if(room_queryData.isEmpty()){
                      room_roomData.setRoom(room);
                      room_roomData.setPassword(password);

                      room_roomData.setEmoticon(emoticon);

                      room_queryData.add(room_roomData);
                      view.setAdapter();
                      view.setArrayList();
                      view.getDataSuccess(room_queryData, childEventListenerDataRoom);

                  }else {

                      for (int i = 0; i <room_queryData.size(); i++){
                          if(room.contentEquals(room_queryData.get(i).getRoom())) {
                              countCheckRoom++;
                              break;
                          }
                      }
                      if(countCheckRoom == 0){
                          //Log.e("KEY","IF " + key);
                          room_roomData.setRoom(room);
                          room_roomData.setPassword(password);

                          room_roomData.setEmoticon(emoticon);

                          room_queryData.add(room_roomData);
                          view.setAdapter();
                          view.setArrayList();
                          view.getDataSuccess(room_queryData, childEventListenerDataRoom);
                      }else{
                          countCheckRoom = 0;
                      }
                  }
              }
          }
          @Override
          public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
          }
          @Override
          public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
              Log.e("REMOVE","Room2");
              for(int i = 0; i < room_addRoom.size(); i++){
                  if(room.contentEquals(room_addRoom.get(i).getRoom())){
                      room_addRoom.remove(i);
                  }
              }
              for(int i = 0; i < room_queryData.size(); i++){
                  if(room.contentEquals(room_queryData.get(i).getRoom())){
                      room_queryData.remove(i);
                  }
              }
              view.getDataSuccess(room_queryData,childEventListenerDataRoom);
          }
          @Override
          public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

          }
          @Override
          public void onCancelled(@NonNull DatabaseError databaseError) {

          }
      });
    }

    @Override
    public void addRoom(final String room, final String password) {
        int countCheck = 0;
        try {
            mUsername = RealmManage.getInstance().getRealmData().get(0).getUsername();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (room_addRoom.isEmpty()) {
            mDataRoom.child(room).child("Password").setValue(password);
            mDataRoom.child(room).child("User").child(mUsername).setValue("");
            mDataRoom.child(room).child("Emoticon").setValue("☺");
        } else {
            for (int i = 0; i < room_addRoom.size(); i++) {
                if (room.contentEquals(room_addRoom.get(i).getRoom())) {
                    countCheck++;
                    break;
                }
            }

            if(countCheck >0){
                view.addRoomFail();
            }else{
                if (!room.isEmpty()) {
                    mDataRoom.child(room).child("Password").setValue(password);
                    mDataRoom.child(room).child("User").child(mUsername).setValue("");
                    mDataRoom.child(room).child("Emoticon").setValue("☺");
                }
            }
        }
    }

    @Override
    public void checkPassword(String passwordOne, String passwordTwo) {

        if(passwordOne.equals(passwordTwo)){

            view.checkPasswordSuccess();
        }else {

            view.checkPasswordSFail();
        }
    }

    @Override
    public void delRoom(String room) {
        mDataRoom.child(room).removeValue();
    }


}
