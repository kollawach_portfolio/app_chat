package com.example.petch.testfirebase.realm;


import com.example.petch.testfirebase.model.RealmDataModel;

import java.util.ArrayList;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class RealmManage {

    private Realm realm;
    private static RealmManage instance;


    public RealmManage() {
        realm = Realm.getDefaultInstance();
    }

    public static RealmManage getInstance() {
        if (instance == null) {
            instance = new RealmManage();
        }

        return instance;
    }

    public Realm getRealm() {
        return realm;
    }


    public void addRealmData(final String key, final String username, final String password, final String deviceId) {

        RealmDataModel realm_data = new RealmDataModel();
        realm.beginTransaction();
        realm_data.setKey(key);
        realm_data.setUsername(username);
        realm_data.setPassword(password);
        realm_data.setDeviceId(deviceId);
        realm.insert(realm_data);
        realm.commitTransaction();
    }

    public void clearAll() {

        realm.beginTransaction();
        realm.delete(RealmDataModel.class);
        realm.commitTransaction();
    }


    //get All
    public ArrayList<RealmDataModel> getRealmData() {

        ArrayList<RealmDataModel> list = new ArrayList<>();

        try {
            RealmResults<RealmDataModel> results = realm.where(RealmDataModel.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } catch (Exception e) {

        }

        return list;
    }
}

