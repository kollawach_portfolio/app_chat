package com.example.petch.testfirebase.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.example.petch.testfirebase.realm.RealmManage;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DestroyAppService extends Service{

    private FirebaseAuth firebaseAuth;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        try {
            String username = firebaseAuth.getCurrentUser().    getDisplayName();
            if ((RealmManage.getInstance().getRealmData().isEmpty() || RealmManage.getInstance().getRealmData().get(0).getPassword().isEmpty()) && username.isEmpty()) {
                String key = RealmManage.getInstance().getRealmData().get(0).getKey();
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
                databaseReference.child(key).child("DeviceId").setValue(null);
                databaseReference.child(key).child("DeviceToken").setValue(null);
                RealmManage.getInstance().clearAll();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        stopSelf();
    }

}
