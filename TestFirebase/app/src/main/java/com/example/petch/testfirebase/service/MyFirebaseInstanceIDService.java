package com.example.petch.testfirebase.service;

import android.util.Log;
import com.example.petch.testfirebase.realm.RealmManage;
import com.firebase.client.Firebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.util.Map;

public class MyFirebaseInstanceIDService extends FirebaseMessagingService {


    private static final String TAG = "MyFirebaseIIDService";
    private String deviceId = "";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        if (s != null) {
            Log.d(TAG, "Refreshed token: " + s);
            // If you want to send messages to this application instance or
            // manage this apps subscriptions on the server side, send the
            // Instance ID token to your app server.
            sendTokenToServer(s);
        }
    }

    public void sendTokenToServer(final String strToken) {
        // API call to send token to Server
        Log.e("MESSAGE","0");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference usersRef = database.getReference("User");

        try {
            deviceId = RealmManage.getInstance().getRealmData().get(0).getDeviceId();
        }catch (Exception e){
            e.printStackTrace();
        }

        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    //Getting the data from snapshot

                    try {
                        Map<String, Object> newPost = (Map<String, Object>) userSnapshot.getValue();
                        String deviceIds = newPost.get("DeviceId").toString();
                        String deviceToken = newPost.get("DeviceToken").toString();

                        Log.e("MESSAGE0", userSnapshot.getKey());
                        if (strToken != null && deviceIds.equals(deviceId) && !strToken.equals(deviceToken)) {

                            Log.e("MESSAGE", userSnapshot.getKey());

                            usersRef.child(userSnapshot.getKey()).child("deviceToken").setValue(strToken, new Firebase.CompletionListener() {

                                @Override
                                public void onComplete(com.firebase.client.FirebaseError firebaseError, Firebase firebase) {
                                    if (firebaseError != null) {
                                        Log.i(TAG, firebaseError.toString());
                                    } else {
                                        System.out.println("Refreshed Token Updated");
                                    }
                                }

                            });

                        }
                    } catch (Exception e) {

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
        }

    });

    }

}
