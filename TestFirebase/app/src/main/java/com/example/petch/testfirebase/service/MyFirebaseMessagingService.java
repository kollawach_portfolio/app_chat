package com.example.petch.testfirebase.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.petch.testfirebase.R;
import com.example.petch.testfirebase.module.manage.ManageActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService  extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.e(TAG, "Notification Message Title: " + remoteMessage.getNotification().getTitle());
        Log.e(TAG, "Notification Message Tag: " + remoteMessage.getNotification().getTag());
        Log.e(TAG, "Notification Message BodyKey: " + remoteMessage.getNotification().getBodyLocalizationKey());
        Log.e(TAG, "Notification Message TitleKey: " + remoteMessage.getNotification().getTitleLocalizationKey());
        Log.e(TAG, "Notification Message DATA: " + remoteMessage.getData());


        Log.e(TAG, "Message getMessageId: " + remoteMessage.getMessageId());
        Log.e(TAG, "Message getData: " + remoteMessage.getData().get("notification"));
        Log.e(TAG, "Message getCollapseKey: " + remoteMessage.getCollapseKey());
        Log.e(TAG, "Message getTo: " + remoteMessage.getTo());
        Log.e(TAG, "Message getNotification: " + remoteMessage.getNotification());


        try{
            Map<String, String> data = remoteMessage.getData();
            Log.e(TAG, "Message getNotification: " + data.get("test"));

        }catch (Exception e){
            e.printStackTrace();
        }
        //Calling method to show notification

        showNotification(remoteMessage.getNotification().getBody());
    }



    private void showNotification(String messageBody) {

        Intent intent = new Intent(this, ManageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(0,notificationBuilder.build());
    }

}
